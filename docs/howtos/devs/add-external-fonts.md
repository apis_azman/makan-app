### How to add external fonts into the project ###

**1)** Make sure `rnpm` is already installed. If belum run `npm install -g rnpm` to intall globally, or `npm install rnpm --save-dev` to install as the project dev's dependency. For Makan, check in `rnpm` is listed as one of the dev dependency in `project.json`, then run `npm install`

**2)** Put the external fonts `.ttf` files in `src/assets/fonts`

**3)** Add `rnpm` configuration to add the fonts in `package.json`
```
{
  "rnpm": {
    "assets": [
      "src/assets/fonts"
    ]
  }
}
```

**4)** Run `rnpm link assets`, this will link the stuffs in `src/assets/fonts` into the Android and iOS packages

**5)** To call the text in code, simply call the font by their file name. Contoh:
```
{
  'fontFamily': 'Jawi-Khat'
}
```