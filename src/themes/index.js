import colors from './Colors'
import fonts from './Fonts'
import images from './Images'

export { colors, fonts, images }