export default {
  main: '#a42065',
  mainGradientEnd: '#09161f',
  mainDark: '#432b3f',
  accent1: '#DC2878',
  accent2: '#f57365',
  placeholder: '#bfbfbf',

  white: '#FFFFFF',
  black: '#000000',
  aquaBlue: '#4db8ff',
  
  lightDark: '#333333',
  lightGrey: '#dddddd',
  lightGreen: '#00b300',
  transparent: 'rgba(0,0,0,0)',
  semiTransparent: 'rgba(255,255,255,0.5)',
  semiTransparentDark: 'rgba(0,0,0,0.3)',
  transparent: 'transparent',
  fbButton: '#385499',
  googleButton:'#E24825',
  emailButton: '#2c3e50'
}