const images = {
  // category backgrounds
  nasiCategoryBackground: require('../assets/images/nasi-background.jpg'),
  meeCategoryBackground: require('../assets/images/mee-background.jpg'),
  bihunCategoryBackground: require('../assets/images/bihun-background.jpg'),
  kueytiowCategoryBackground: require('../assets/images/kueytiow-background.jpg'),
  airCategoryBackground: require('../assets/images/air-background.jpg'),
  indianCategoryBackground: require('../assets/images/indian-background.jpg'),
  japaneseCategoryBackground: require('../assets/images/japanese-background.jpg'),
  westernCategoryBackground: require('../assets/images/western-background.jpg')
}

export default images