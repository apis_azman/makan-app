const type = {
  regular: 'OpenSans-Regular',
  italic: 'OpenSans-Italic',
  semibold: 'OpenSans-Semibold',
  bold: 'OpenSans-Bold'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  regular: 16,
  medium: 14,
  small: 12,
  tiny: 10
}

const style = {
  pageTitle: {
    fontFamily: type.semibold,
    fontSize: size.h6,
    textAlign: 'center',
    color: 'white'
  }
}

export default {
  type,
  size,
  style
}