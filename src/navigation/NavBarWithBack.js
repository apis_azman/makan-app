import React from 'react'
import { Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { colors, fonts } from '../themes/'
import styles from './styles/NavBarsStyle'

import IconButton from '../components/Buttons/IconButton'

class NavBarWithBack extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <IconButton style={styles.icon} iconName='arrow-back' iconSize={21} iconColor={colors.white} action={Actions.pop} />
        <View style={styles.pageTitleContainer}>
          <Text style={fonts.style.pageTitle}>{ this.props.pageTitle }</Text>
        </View>
      </View>
    )
  }
}

export default NavBarWithBack