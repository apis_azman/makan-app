import React from 'react'
import { View, Text } from 'react-native'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'
import { NavBarWithBack } from './NavBars'

import ChatList from '../containers/Chat/ChatList'

import styles from './styles/TabHomeRouterStyle'

const firstTab = () => <ChatList /> // All
const secondTab = () => <ChatList /> // Buy
const thirdTab = () => <ChatList /> // Sell

export default class TabChatInboxRouter extends React.PureComponent {
  state = {
    index: 0,
    routes: [
      { key: '1', title: 'All' },
      { key: '2', title: 'Buy' },
      { key: '3', title: 'Sell' }
    ]
  }
  handleTabChange = index => {
    this.setState({ index })
  }
  renderScene = SceneMap({
    '1': firstTab,
    '2': secondTab,
    '3': thirdTab
  })
  renderHeader = props => {
    return (
      <View style={styles.headerContainer}>
        <NavBarWithBack pageTitle="Chat Inbox" />
        <TabBar
          {...props}
          style={styles.tab}
          indicatorStyle={styles.tabIndicator}
          labelStyle={styles.tabLabel}
        />
      </View>
    )
  }
  render() {
    return (
      <TabViewAnimated 
        navigationState={this.state}
        renderScene={this.renderScene}
        renderHeader={this.renderHeader}
        onRequestChangeTab={this.handleTabChange}
      />
    )
  }
}