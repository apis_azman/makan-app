import React from 'react'
import { Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { colors, fonts } from '../themes/'
import styles from './styles/NavBarsStyle'

import IconButton from '../components/Buttons/IconButton'

const openChatList = () => {
  Actions.chatList({page: 'Inbox'})
}

class MainNavBarWithBack extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <IconButton style={styles.icon} iconName='arrow-back' iconSize={21} iconColor={colors.white} action={Actions.pop} />
        <IconButton style={styles.icon} iconName='search' iconSize={21} iconColor={colors.white} />
        <View style={styles.pageTitleContainer}>
          <Text style={fonts.style.pageTitle}>{ this.props.pageTitle }</Text>
        </View>
        <IconButton  style={styles.icon} iconName='favorite-border' iconSize={21} iconColor={colors.white} />
        <IconButton  style={styles.icon} iconName='chat-bubble-outline' iconSize={21} iconColor={colors.white} action={openChatList} />
      </View>
    )
  }
}

export default MainNavBarWithBack