import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../themes/'

export default {
  headerContainer: {
    elevation: 5, 
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colors.main,
    shadowColor: 'black',
    shadowOpacity: 0.1,
    shadowRadius: StyleSheet.hairlineWidth,
    shadowOffset: {
      height: StyleSheet.hairlineWidth,
    }
  },
  tab: {
    backgroundColor: colors.main,
    elevation: 0,
    shadowColor: colors.transparent
  },
  tabIndicator : {
    backgroundColor: colors.white
  },
  tabLabel: {
    fontSize: fonts.size.small
  }
}