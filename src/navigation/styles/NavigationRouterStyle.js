import { colors } from '../../themes/'

export default {
  tabBar: {
    height: 60,
    backgroundColor: colors.mainGradientEnd
  }
}