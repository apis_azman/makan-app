import { StyleSheet } from 'react-native'

import { colors }from '../../themes'

export default StyleSheet.create({
  container: {
    backgroundColor: colors.main,
    height: 50,
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  pageTitleContainer: {
    marginLeft: 10, // temp, remove this line when search icon is added back
    flex: 4,
    justifyContent: 'center',
    paddingLeft: 10,
    alignItems: 'flex-start'
  },
  icon: {
    flex: 1
  },
  iconSeparator: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 5
  }
})