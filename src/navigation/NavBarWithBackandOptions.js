import React from 'react'
import { View, Text } from 'react-native'
import { Actions } from 'react-native-router-flux'

import styles from './styles/NavBarsStyle'
import { fonts } from '../themes'

import IconButton from '../components/Buttons/IconButton'

class NavBarWithBackandOptions extends React.Component {
  render() {
    return (
      <View style={[styles.container, { backgroundColor: this.props.backgroundColor }]}>
        <IconButton iconName='arrow-back' iconSize={21} iconColor={this.props.iconColor} action={Actions.pop} />
        <View style={styles.iconSeparator}>
          { this.props.pageTitle ? <Text style={fonts.style.pageTitle}>{ this.props.pageTitle }</Text> : null }
        </View>
        <IconButton iconName='more-vert' iconSize={21} iconColor={this.props.iconColor} action={this.props.optionsOnClick} />
      </View>
    )
  }
}

export default NavBarWithBackandOptions