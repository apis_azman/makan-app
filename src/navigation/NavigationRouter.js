import React from 'react'
import { Text, AsyncStorage, View } from 'react-native'
import { Actions, ActionConst, Router, Scene } from 'react-native-router-flux'
import { connect } from 'react-redux'

import { loginExistingUser } from '../redux/actions/accountAction';

import styles from './styles/NavigationRouterStyle'
import rootStyles from '../containers/styles/RootContainerStyle'
import { colors } from '../themes/'

import PageBackground from '../components/PageBackground'
import TabHomeRouter from './TabHomeRouter'
import FoodCatalog from '../containers/Food/FoodCatalog'
import FoodDetails from '../containers/Food/FoodDetails'
import LoginPage from '../containers/Login/LoginPage'
import LoginEmailPage from '../containers/Login/LoginEmailPage'
import SignupPage from '../containers/Login/SignupPage'
import MenuOption from '../components/MenuOption'
import EditProfile from '../containers/UserProfile/EditProfile'
import Following from '../containers/UserProfile/Following'
import AddMenu from '../containers/Food/AddMenu'
import SetupShop from '../containers/ShopProfile/SetupShop'
import TabChatInboxRouter from './TabChatInboxRouter'
import ChatPage from '../containers/Chat/ChatPage'
import OrderFood from '../containers/Food/OrderFood'
import PickLocation from '../containers/ShopProfile/PickLocation'
import SpinnerOverlay from 'react-native-loading-spinner-overlay'
import ShopProfile from '../containers/ShopProfile/ShopProfile'

class NavigationRouter extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      loading: true,
    }
  }

  componentWillMount() {
    var self = this;
    
    AsyncStorage.getItem('jwtToken')
      .then((token) => {
        AsyncStorage.getItem('userEmail')
          .then((userEmail) => {
            AsyncStorage.getItem('userID')
              .then((userID) => {
                if(token && userID && userEmail) {
                  self.props.loginExistingUser({ userID, userEmail, token})
                  self.setState({
                    loading: false
                  })
                } else {
                  self.setState({
                    loading: false,
                  })
                  Actions.tabLogin()
                }
              })
          })
      })
  }

  render() {
    if (this.state.loading) {
      return (
          <SpinnerOverlay visible overlayColor={colors.semiTransparentDark} />
      )
    }

    return (
      <Router>
        <Scene key="root">
            <Scene key='main' component={PageBackground} title='Main' initial type={ActionConst.RESET} panHandlers={null} hideNavBar />

            <Scene key='tabLogin' component={LoginPage} title='Login' hideNavBar />
            <Scene key='tabLoginEmail' component={LoginEmailPage} title='LoginEmail' hideNavBar  />
            <Scene key='tabSignup' component={SignupPage} title='Signup' hideNavBar  />
            
            <Scene key='tabHome' component={TabHomeRouter} title='Home' hideNavBar />
            <Scene key='foodCatalog' component={FoodCatalog} title='Food Catalog' hideNavBar />
            <Scene key='foodDetails' component={FoodDetails} title='Food Details' hideNavBar />
            <Scene key='menuOption' component={MenuOption} title='Menu Option' hideNavBar />
            <Scene key='editProfile' component={EditProfile} title='Edit Profile' hideNavBar />
            <Scene key='following' component={Following} title='Following' hideNavBar />
            <Scene key='chatList' component={TabChatInboxRouter} title='Chat Inbox' hideNavBar />
            <Scene key='chatPage' component={ChatPage} title='Chat' hideNavBar />
            <Scene key='setupShop' component={SetupShop} title='Setup Shop' hideNavBar />
            <Scene key='addMenu' component={AddMenu} title='Add Menu' hideNavBar />
            <Scene key='orderFood' component={OrderFood} title='Order Food' hideNavBar />
            <Scene key='pickLocation' component={PickLocation} title='Pick Location' hideNavBar />

            <Scene key='shopProfile' component={ShopProfile} title='Shop Profile' hideNavBar />
        </Scene>        
      </Router>
    )
  }
}

export default connect(null, { loginExistingUser })(NavigationRouter);