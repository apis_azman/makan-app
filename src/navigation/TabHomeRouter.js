import React from 'react'
import { View, Text } from 'react-native'
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'
import IconBadge from 'react-native-icon-badge'
import { connect } from 'react-redux'

import { setNavContent } from '../redux/actions/navBarAction'

import { MainNavBar } from './NavBars'
import HomeScreen from '../containers/Landing/HomeScreen'
import Kitchen from '../containers/Landing/Kitchen'
import Activity from '../containers/Landing/Activity'
import Profile from '../containers/Landing/Profile'

import styles from './styles/TabHomeRouterStyle'

const mapStateToProps = ({ accountReducer }) => ({
  store: accountReducer.store
})

const mapDispatchToProps = (dispatch) => ({
  setNavContent: (pageTitle, showMode) => dispatch(setNavContent(pageTitle, showMode))
})

const firstTab = () => <HomeScreen />
const secondTab = () => <Kitchen />
const thirdTab = () => <Activity />
const fourthTab = () => <Profile />

class TabHomeRouter extends React.PureComponent {
  constructor(props) {
    super(props);
    let index = this.props.index ? this.props.index : 0

    this.state = {
      index: index,
      routes: this.props.store ? [{ key: '1', title: 'Browse' }, { key: '2', title: 'Kitchen' }, { key: '3', title: 'Activity' }, { key: '4', title: 'Profile' }] : [{ key: '1', title: 'Browse' }, { key: '3', title: 'Activity' }, { key: '4', title: 'Profile' }]
    }
  }
  handleTabChange = index => {
    this.setState({ index })

    if(this.props.store)
      switch(this.state.index) {
        case 0:
          this.props.setNavContent('Nearby Foods', true)
          break
        case 1:
          this.props.setNavContent('Chopboard', false)
          break
        case 2:
          this.props.setNavContent('Activity', false)
          break
        case 3:
          this.props.setNavContent('Profile', false)
          break
        default:
          break
      }
    else
      switch(this.state.index) {
        case 0:
          this.props.setNavContent('Nearby Foods', true)
          break
        case 1:
          this.props.setNavContent('Activity', false)
          break
        case 2:
          this.props.setNavContent('Profile', false)
          break
        default:
          break
      }
  }
  renderScene = SceneMap({
    '1': firstTab,
    '2': secondTab,
    '3': thirdTab,
    '4': fourthTab
  })
  renderHeader = props => { 
    return (
      <View style={styles.headerContainer}>
        <MainNavBar />
        <TabBar 
          {...props} 
          style={styles.tab}
          indicatorStyle={styles.tabIndicator}
          labelStyle={styles.tabLabel}
        />
      </View>
    )
  }
  render() {
    return (
      <TabViewAnimated 
        navigationState={this.state}
        renderScene={this.renderScene}
        renderHeader={this.renderHeader}
        onRequestChangeTab={this.handleTabChange}
      />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TabHomeRouter)