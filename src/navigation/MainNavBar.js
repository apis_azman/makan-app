import React from 'react'
import { 
  Text, 
  View, 
  TouchableOpacity 
} from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import { setHomeScreenMode } from '../redux/actions/homeScreenModeAction'

import { colors, fonts } from '../themes/'
import styles from './styles/NavBarsStyle'

import IconButton from '../components/Buttons/IconButton'

const mapStateToProps = ({ homeScreenModeReducer, navBarReducer }) => ({
  homeScreenMode: homeScreenModeReducer.homeScreenMode,
  pageTitle: navBarReducer.pageTitle,
  showMode: navBarReducer.showMode
})
const mapDispatchToProps = (dispatch) => ({
  setHomeScreenMode: () => { dispatch(setHomeScreenMode()) }
})

const openChatList = () => {
  Actions.chatList({page: 'Inbox'})
}

class MainNavBar extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <IconButton style={styles.icon} iconName='search' iconSize={25} iconColor={colors.white} />
        <View style={styles.pageTitleContainer}>
          <Text style={fonts.style.pageTitle}>{ this.props.pageTitle }</Text>
        </View>
        { this.props.showMode ? (<IconButton style={styles.icon} iconName={this.props.homeScreenMode == 'Categories'? 'map' : 'apps'} iconSize={25} iconColor={colors.white} action={this.props.setHomeScreenMode} />) : <IconButton iconSize={25} /> }
        <IconButton  style={styles.icon} iconName='favorite-border' iconSize={25} iconColor={colors.white} />
        <IconButton style={styles.icon} iconName='chat-bubble-outline' iconSize={25} iconColor={colors.white} action={openChatList} />
      </View>
    )
  }
}

export default connect(mapStateToProps, { setHomeScreenMode })(MainNavBar)
