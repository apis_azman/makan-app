import core from './core'

createMenuApi = core.apis.createMenu
getCourseApi = core.apis.getCourse
getCategoryApi = core.apis.getCategory
getMenuApi = core.apis.getMenu

function createMenu(menu, auth, callback) {
  // helper to get the available days
  var getAvailableDays = function(availableDays) {
    var days = []
    for(var day in availableDays)
      if (availableDays[day] && availableDays.hasOwnProperty(day))
        days.push(day)
    return days
  }
  // helper to get the availability time
  var getAvailableTime = function(availableFrom, availableTo) {
    var availability = {}
    if(availableFrom || availableTo)
      availability.time = {}
    if(availableFrom)
      availability.time.from = parseFloat(availableFrom)
    if(availableTo)
      availability.time.to = parseFloat(availableTo)

    return availability
  }

  // convert preptime
  var prepTime = 0
  if(menu.prepTime.charAt(0) == '1')
    prepTime = 60
  else
    prepTime = parseInt(menu.prepTime.substring(0, menu.prepTime.indexOf(' ')))

  // set availability
  var availablty = getAvailableTime(menu.availableFrom, menu.availableTo)
  availablty.day = getAvailableDays(menu.availableDays)
  availablty.allTime = menu.allTime

  body = {
    'name': menu.title,
    'price': parseFloat(menu.price),
    'course': menu.course,
    'imageId': menu.photo,
    'description': menu.description,
    'halal': menu.halal,
    'availability': availablty,
    'deliveryInfo': {
      'cod': menu.delivery,
      'prepTime': prepTime
    },
    'category': menu.category,
    'tags': menu.tags.split(" ")
  }
  storeId = menu.store._id
  core.apiPost(createMenuApi + storeId, body, auth, function(res) {
    callback(res)
  })
  callback('true');
}

function getCourses(auth, callback) {
  core.apiGet(getCourseApi, auth, function(res) {
    callback(res)
  })
}

function getCategories(auth, callback) {
  core.apiGet(getCategoryApi, auth, function(res) {
    callback(res)
  })
}

function getMenu(auth, callback) {
  core.apiGet(getMenuApi, auth, function(res) {
    callback(res)
  })
}




export default {
  createMenu: createMenu,
  getCourses: getCourses,
  getCategories: getCategories,
  getMenu: getMenu
}