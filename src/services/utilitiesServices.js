import core from './core'

uploadApi = core.apis.upload
updateUploadApi = core.apis.uploadUpdate

function uploadFile(file, auth, callback) {
  if(!file) {
    callback(null)
  }
  else {
    var formData = new FormData()
    formData.append('file', { uri: file.uri, type: file.type, name: file.fileName })
    core.apiPostFormData(uploadApi, formData, auth, function(res) {
      callback(res)
    })
  }
}

function updateUploadFile(file, imageId, auth, callback) {
  if(!file)
    callback(null)
  else {
    var formData = new FormData()
    formData.append('file', { uri: file.uri, type: file.type, name: file.fileName })
    core.apiPutFormData(updateUploadApi + '/' + imageId, formData, auth, function(res) {
      callback(res)
    })
  }
}

export default {
  uploadFile: uploadFile,
  updateUploadFile: updateUploadFile
}