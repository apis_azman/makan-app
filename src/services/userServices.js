import core from './core'

userApi = core.apis.user
registerApi = core.apis.register
loginApi = core.apis.login
updateApi = core.apis.update
fbAuthApi = core.apis.fbAuth
googleAuthApi = core.apis.googleAuth

function register(name, email, password, passwordConfirm, callback) {
  body = {
    'name': name,
    'email': email,
    'password': password,
    'passwordConfirmation': passwordConfirm
  }
  core.apiPost(registerApi, body, '', function(res) {
    callback(res)
  })
}

function login(email, password, callback) {
  body = {
    'email': email,
    'password': password
  }
  core.apiPost(loginApi, body, '', function(res) {
    callback(res)
  })
}

function getUser(userId, auth, callback) {
  core.apiGet(userApi + '/' + userId, auth, function(res) {
    callback(res)
  })
}

function updateUser(userId, data, auth, callback) {
  body = {
    name: data.name,
    email: data.email,
    phone: data.phone,
    address: data.address,
    gender: data.gender
  }
  core.apiPut(updateApi, body, auth, function(res) {
    callback(res)
  })
}

function fbLogin(token, callback) {
  body = {
    'access_token': token
  }

  core.apiPost(fbAuthApi, body, '', function(res) {
    callback(res);
  })
}

function googleLogin(token, callback) {
  body = {
    'access_token': token
  }

  core.apiPost(googleAuthApi, body, '', function(res) {
    callback(res);
  })
}

export default {
  register: register,
  login: login,
  getUser: getUser,
  updateUser: updateUser,
  fbLogin: fbLogin,
  googleLogin: googleLogin
}