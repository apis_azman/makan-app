import { reduxStore } from '../containers/App'
import { showSpinnerOverlay } from '../redux/actions/showSpinnerOverlayAction'

hostUrl = 'http://makan.asticus.com.my:3000/api/v1/'

// API URLS
apis = {
  // user
  user: 'user',
  register: 'user/register',
  login: 'user/login',
  update: 'user/update',

  // auth
  fbAuth: 'auth/facebook/token',
  googleAuth: 'auth/google/token',

  // store
  store: 'store',
  getStorebyUserId: 'store?userId=',
  getStorebyStoreId: 'store/',
  createStore: 'store/create?userId=',

  // menu
  createMenu: 'menu/create?storeId=',
  getCourse: 'course',
  getCategory: 'category',
  getMenu: 'menu',


  // utilites
  upload: 'upload',
  uploadUpdate: 'upload/update'
}

// GET
function apiGet(url, auth, callback) {
  reduxStore.dispatch(showSpinnerOverlay(true))
  fetch(hostUrl + url, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': auth
    }
  }).then(res => res.json()).then(data => { 
    reduxStore.dispatch(showSpinnerOverlay(false))
    callback(data) 
  })
}


// PUT
function apiPut(url, body, auth, callback) {
  reduxStore.dispatch(showSpinnerOverlay(true))
  fetch(hostUrl + url, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': auth
    },
    body: JSON.stringify(body)
  }).then(res => res.json()).then(data => { 
    reduxStore.dispatch(showSpinnerOverlay(false))
    callback(data) 
  })
}
function apiPutFormData(url, formData, auth, callback) {
  reduxStore.dispatch(showSpinnerOverlay(true))
  fetch(hostUrl + url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'multipart/form-data',
      'authorization': auth
    },
    body: formData
  }).then(res => res.json()).then(data => {
    reduxStore.dispatch(showSpinnerOverlay(false))
    callback(data) 
  })
}


// POST
function apiPost(url, body, auth, callback) {
  reduxStore.dispatch(showSpinnerOverlay(true))
  fetch(hostUrl + url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': auth
    },
    body: JSON.stringify(body)
  }).then(res => res.json()).then(data => { 
    reduxStore.dispatch(showSpinnerOverlay(false))
    callback(data) 
  })
}
function apiPostFormData(url, formData, auth, callback) {
  reduxStore.dispatch(showSpinnerOverlay(true))
  fetch(hostUrl + url, {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      'authorization': auth
    },
    body: formData
  }).then(res => res.json()).then(data => {
    reduxStore.dispatch(showSpinnerOverlay(false))
    callback(data) 
  })
}


// DELETE
function apiDelete(url) {
  reduxStore.dispatch(showSpinnerOverlay(true))
}

export default {
  apis: apis,
  apiGet: apiGet,
  apiPut: apiPut,
  apiPutFormData: apiPutFormData,
  apiPost: apiPost,
  apiPostFormData:apiPostFormData,
  apiDelete: apiDelete
}