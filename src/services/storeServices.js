import core from './core'

storeApi = core.apis.store
getStoreByUserIdApi = core.apis.getStorebyUserId
getStorebyStoreIdApi = core.apis.getStorebyStoreId
createStoreApi = core.apis.createStore


function getStoreByUserId(userId, auth, callback) {
  core.apiGet(getStoreByUserIdApi + userId, auth, function(res) {
    callback(res)
  })
}

function getStoreByStoreId(storeId, auth, callback) {
  core.apiGet(getStorebyStoreIdApi + storeId, auth, function(res) {
    callback(res)
  })
}

function setupStore(storeData, auth, callback) {
  body = {
    'name': storeData.name,
    'description': storeData.description,
    'imageId': storeData.photo,
    'address': {
      'address_1': storeData.addr1,
      'address_2': storeData.addr2,
      'city': storeData.city,
      'postcode': storeData.postcode,
      'state': storeData.state
    },
    'operatingModel': {
      'operationHours': {
        'from': parseFloat(storeData.openingFrom),
        'to': parseFloat(storeData.openingTo)
      },
      'cod': storeData.delivery
    }
  }
  userId = storeData.user.id
  core.apiPost(createStoreApi + userId, body, auth, function(res) {
    callback(res)
  })
}

function updateStore(storeData, auth, callback) {
  body = {
    'name': storeData.name,
    'description': storeData.description,
    'imageId': storeData.photo,
    'address': {
      'address_1': storeData.addr1,
      'address_2': storeData.addr2,
      'city': storeData.city,
      'postcode': storeData.postcode,
      'state': storeData.state
    },
    'operatingModel': {
      'operationHours': {
        'from': parseFloat(storeData.openingFrom),
        'to': parseFloat(storeData.openingTo)
      },
      'cod': storeData.delivery
    }
  }
  console.log(body)
  userId = storeData.user.id
  core.apiPut(storeApi + '/' + storeData.storeId, body, auth, function(res) {
    callback(res)
  })
}



export default {
  getStoreByUserId: getStoreByUserId,
  getStoreByStoreId: getStoreByStoreId,
  setupStore: setupStore,
  updateStore: updateStore
}