import { 
  LOGIN_USER_SUCCESS, 
  LOGIN_USER_FAIL, 
  LOGIN_USER, 
  SIGNUP_USER_FAIL, 
  SIGNUP_CREATE, 
  SIGNUP_USER_SUCCESS,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAIL,
  GOOGLE_LOGIN_SUCCESS,
  GOOGLE_LOGIN_FAIL,

  SET_STORE_DETAILS

} from '../actions/accountAction'

const initialState = { 
  user: '',
  store: '',
  token: '',
  menu: '',
  error: '',
  loading: false
}

export default (state = initialState, action) => {
  switch(action.type) {
    case LOGIN_USER_SUCCESS:
      return { ...state, user: action.user, token: action.token }
    case LOGIN_USER_FAIL:
      return { ...state, error: action.payload, loading: false }
    case LOGIN_USER:
      return { ...state, loading: true, error: '' }
    case SIGNUP_USER_FAIL:
      return { ...state, error: action.payload, loading: false }
    case SIGNUP_CREATE:
      return initialState
    case SIGNUP_USER_SUCCESS:
      return { ...state, user: action.user, token: action.token }
    case FB_LOGIN_SUCCESS:
      return { ...state, token: action.token, user: action.user }
    case FB_LOGIN_FAIL:
      return state
    case GOOGLE_LOGIN_SUCCESS:
      return { ...state, token: action.token, user: action.user }
    case GOOGLE_LOGIN_FAIL:
      return state

    case SET_STORE_DETAILS: {
      return { ...state, store: action.store }
    }

    default:
      return state
  }
}