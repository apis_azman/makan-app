import { combineReducers } from 'redux'

import navBarReducer from './navBarReducer'
import homeScreenModeReducer from './homeScreenModeReducer'
import alertModalReducer from './alertModalReducer'
import showSpinnerOverlayReducer from './showSpinnerOverlayReducer'
import accountReducer from './accountReducer'

const appReducers = combineReducers({
  navBarReducer,
  homeScreenModeReducer,
  alertModalReducer,
  showSpinnerOverlayReducer,
  accountReducer
  // fbAuth: () => { return {} }
})

export default appReducers