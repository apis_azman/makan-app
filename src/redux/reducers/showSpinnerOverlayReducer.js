import { SHOW_SPINNER_OVERLAY } from '../actions/showSpinnerOverlayAction'

const initialState = {
  showSpinnerOverlay: false
}

export default function setShowSpinnerOverlay(state = initialState, action) {
  switch(action.type) {
    case SHOW_SPINNER_OVERLAY:
      return Object.assign({}, state, {
        showSpinnerOverlay: action.showSpinnerOverlay
      })
    default:
      return state
  }
}