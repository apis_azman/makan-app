import { SHOW_ALERT_MODAL } from '../actions/alertModalAction'

const initialState = {
  message: '',
  showAlertModal: false
}

export default function setShowAlertModal(state = initialState, action) {
  switch(action.type) {
    case SHOW_ALERT_MODAL:
      return Object.assign({}, state, {
        message: action.message,
        showAlertModal: action.showAlertModal
      })
    default:
      return state
  }
}