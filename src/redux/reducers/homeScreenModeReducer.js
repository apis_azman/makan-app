import { SET_HOMESCREEN_MODE } from '../actions/homeScreenModeAction'

const initialState = {
  homeScreenMode: 'Map'
}

export default function setHomeScreenMode(state = initialState, action) {
  switch(action.type) {
    case SET_HOMESCREEN_MODE:
      return Object.assign({}, state, {
        homeScreenMode: state.homeScreenMode == 'Categories' ? 'Map' : 'Categories'
      })
    default:
      return state
  }
}