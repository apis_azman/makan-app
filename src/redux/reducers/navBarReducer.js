import { SET_NAV_CONTENT } from '../actions/navBarAction'

const initialState = {
  pageTitle: 'Nearby Foods',
  showMode: true
}

export default function setNavContent(state = initialState, action) {
  switch(action.type) {
    case SET_NAV_CONTENT:
      return Object.assign({}, state, {
        pageTitle: action.pageTitle,
        showMode: action.showMode
      })
    default:
      return state
  }
}