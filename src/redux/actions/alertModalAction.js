export const SHOW_ALERT_MODAL = 'SHOW_ALERT_MODAL'

export const showAlertModal = (message, showAlertModal) => ({
  message: message,
  showAlertModal: showAlertModal,
  type: SHOW_ALERT_MODAL
})