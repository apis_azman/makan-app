export const SET_NAV_CONTENT = 'SET_NAV_CONTENT'

export const setNavContent = (pageTitle, showMode) => ({
  pageTitle: pageTitle,
  showMode: showMode,
  type: SET_NAV_CONTENT
})