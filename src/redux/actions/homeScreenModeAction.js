export const SET_HOMESCREEN_MODE = 'SET_HOMESCREEN_MODE'

export const setHomeScreenMode = () => ({
  type: SET_HOMESCREEN_MODE
})