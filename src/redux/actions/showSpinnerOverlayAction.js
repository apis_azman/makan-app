export const SHOW_SPINNER_OVERLAY = 'SHOW_SPINNER_OVERLAY'

export const showSpinnerOverlay = (showSpinnerOverlay) => ({
  showSpinnerOverlay: showSpinnerOverlay,
  type: SHOW_SPINNER_OVERLAY
})