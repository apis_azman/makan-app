import { AsyncStorage } from 'react-native';
import FBSDK, { LoginManager, AccessToken } from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin';
import { Actions } from 'react-native-router-flux'

import userServices from '../../services/userServices'
import storeServices from '../../services/storeServices'

export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAIL = 'login_user_fail'
export const LOGIN_USER = 'login_user'

export const SIGNUP_USER_SUCCESS = 'signup_user_success'
export const SIGNUP_USER_FAIL ='signup_user_fail'
export const SIGNUP_CREATE = 'signup_create'

export const FB_LOGIN_SUCCESS = 'fb_login_success'
export const FB_LOGIN_FAIL = 'fb_login_fail'

export const GOOGLE_LOGIN_SUCCESS = 'google_login_success'
export const GOOGLE_LOGIN_FAIL = 'google_login_fail'

export const SET_STORE_DETAILS = 'SET_STORE_DETAILS'


// EMAIL LOGIN

export const loginUser = (payload) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER })
    userServices.login(payload.email, payload.password, function(data) {
      if(data.error) {
        var errMsg = data.error
        if(data.error.hasOwnProperty('message'))
          errMsg = data.error.message
        else if(data.error.hasOwnProperty('error'))
          errMsg = data.error.error
        loginUserFail(dispatch, errMsg)
      } else {
        loginUserSuccess(dispatch, data.user, data.token)
      }
    })
  }
}

export const signupUser = ({ name, email, password, passwordConfirm }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER })
    userServices.register(name, email, password, passwordConfirm, function(data) {
      console.log(data);
      if(data.error) {
        var errMsg
        if(data.error.error)
          errMsg = data.error.error
        else if (data.error.message)
          errMsg = data.error.message
        else
          errMsg = data.error
        signupUserFail(dispatch, errMsg)
      }
      else
        signupUserSuccess(dispatch, data.user, data.token)
    })
  }
}

export const signupCreate = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: SIGNUP_CREATE })
    Actions.tabSignup();
  }
}

export const dismissAlert = () => {
  return (dispatch) => {
    dispatch({ type: SIGNUP_CREATE })
  }
}

export const loginUserSuccess = (dispatch, user, token) => {
 AsyncStorage.setItem('jwtToken', token);
 AsyncStorage.setItem('userID', user.id);
 AsyncStorage.setItem('userEmail', user.email);
  
  // get user's shop details
  storeServices.getStoreByUserId(user.id, token, function(data) {

    if(!data.error)
      dispatch(setStoreDetails(data.store))

    dispatch({
      type: LOGIN_USER_SUCCESS,
      user: user,
      token: token
    })
      
    Actions.tabHome({type: 'reset'});
  })
}

export const loginExistingUser = ({ userID, userEmail, token }) => {
  return (dispatch) => {
    storeServices.getStoreByUserId(userID, token, function(data) {

      if(!data.error)
        dispatch(setStoreDetails(data.store))

      let newUser = {
        id: userID,
        email: userEmail
      }

      dispatch({
        type: LOGIN_USER_SUCCESS,
        user: newUser,
        token: token,
      })

      Actions.tabHome({type: 'reset'})
    })
  }
}

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error
  })
}

const signupUserSuccess = (dispatch, user, token) => {
  AsyncStorage.setItem('jwtToken', token);
  AsyncStorage.setItem('userID', user.id);
  AsyncStorage.setItem('userEmail', user.email);
  
  dispatch({
    type: SIGNUP_USER_SUCCESS,
    user: user,
    token: token
  })

  dispatch({
    type: LOGIN_USER_SUCCESS,
    user: user,
    token: token
  })

  Actions.tabHome({type: 'reset'});
}

const signupUserFail = (dispatch, error) => {
  dispatch({
    type: SIGNUP_USER_FAIL,
    payload: error
  })
}


// FACEBOOK LOGIN
export const appLogin = () => async dispatch => {
  // await AsyncStorage.clear();
  let keys = await AsyncStorage.getAllKeys();
  console.log('keys below!')
  console.log(keys);
};

var facebookLogin = (dispatch, token) => {
  AsyncStorage.setItem('fb_token', token);

  userServices.fbLogin(token, function(data) {
    console.log(data);

    dispatch({ type: FB_LOGIN_SUCCESS, token: data.token, user: data.user });
    loginUserSuccess(dispatch, data.user, data.token);
  })
}

export const doFacebookLogin = () => async dispatch => {
  let result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);

  if (result.isCancelled) {
    return dispatch({ type: FB_LOGIN_FAIL });
  } else {
    let data = await AccessToken.getCurrentAccessToken();
    let token = data.accessToken;
    
    facebookLogin(dispatch, token);
  }
};

//GOOGLE LOGIN

export const doGoogleLogin = () => async dispatch => {
  let result = await  GoogleSignin.signIn()
  console.log(result);
  if (result){
    let token = result.accessToken;
    
    // googleLogin(dispatch, token);

  } else {
      console.log("Cancelled google signin");
  }
};

var googleLogin = (dispatch, token) => {
  AsyncStorage.setItem('google_token', token);

  userServices.googleLogin(token, function(data) {
    console.log(data);
    if(data.error){
      alert(data.error)
    } else {
      dispatch({ type: GOOGLE_LOGIN_SUCCESS, token: data.token, user: data.user });
      loginUserSuccess(dispatch, data.user, data.token);
    }    
  })
  
}



// STORE

export const setStoreDetails = (store) => ({
  store: store,
  type: SET_STORE_DETAILS
})
