import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'

let calculateDiscount = function(currentPrice, originalPrice) {
  return ((originalPrice - currentPrice) / originalPrice * 100).toFixed(0)
}

let renderRatingStar = function(rating, styles) {
  let ratingStar = []
  for(var i = 1; i <= 5; i++) {
    if(i <= rating)
      ratingStar.push(<Icon key={i} style={styles} name='star' />)

    if(i > rating && (i - rating > 0 && i - rating < 1))
      ratingStar.push(<Icon key={i} style={styles} name='star-half-o' />)
    else if(i > rating)
      ratingStar.push(<Icon key={i} style={styles} name='star-o' />)
  }
  return ratingStar
}

let checkValue = function(value) {
  return value ? value : ''
}

export default {
  checkValue: checkValue,
  calculateDiscount: calculateDiscount,
  renderRatingStar: renderRatingStar
}