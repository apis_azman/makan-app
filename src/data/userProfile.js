const userProfile = {
  name: 'Afonso de Albuquerque',
  profileImage: 'https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/999/UP0101-CUSA01140_00-AV00000000000001/1489891825000/image?_version=00_09_000&platform=chihiro&w=225&h=225&bg_color=000000&opacity=100',
  lastLocation: 'Petaling Jaya, SELANGOR',
  registeredOn: '17/07/2017',
  orders: 9,
  following: 0,
  followers: 1,
  shop: {
    shopName: 'ShellIn!',
    shopImage: 'http://ghk.h-cdn.co/assets/16/38/980x490/landscape-1474822198-how-to-make-pancakes.jpg',
    location: 'Pelating Jaya, Selangor',
    menu: []
  }
}

export default userProfile