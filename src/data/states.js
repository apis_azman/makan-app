const states = [
    'Johor',
    'Kedah',
    'Kelantan',
    'Melaka',
    'Negeri Sembilan',
    'Pahang',
    'Pulau Pinang',
    'Perak',
    'Perlis',
    'Terengganu',
    'Sabah',
    'Sarawak',
    'Selangor',
    'W. P. Kuala Lumpur',
    'W. P. Labuan',
    'W. P. Putrajaya'
  ]

export default states