const followingFoodData = [
  {
    shop: {
      profileImage: 'http://www.oldgas.com/forum/ubbthreads.php?ubb=download&Number=46713&filename=shell%20early%20logo%20design%20jpg.JPG',
      name: 'Shell In!',
      location: 'Petaling Jaya, SELANGOR'
    },
    food: {
      id: '101',
      name: 'Seafood Salad',
      image: 'http://downloadclipart.org/blog/wp-content/uploads/2016/05/Salad-Salad.jpg',
      currentPrice: 19.90,
      originalPrice: '',
      rating: 2.5,
      ratingCount: 225,
      time: 'lunch, dinner',
      isFavorited: true
    }
  },
  {
    shop: {
      profileImage: 'http://www.oldgas.com/forum/ubbthreads.php?ubb=download&Number=46713&filename=shell%20early%20logo%20design%20jpg.JPG',
      name: 'Shell In!',
      location: 'Petaling Jaya, SELANGOR'
    },
    food: {
      id: '102',
      name: 'Homemade Pancakes',
      image: 'https://freshcitymarket.com/sites/lafayette/files/uploads/blog/2015/09/pancakes.jpg',
      currentPrice: 3.90,
      originalPrice: 10.90,
      rating: 3,
      ratingCount: 45,
      time: 'all day',
      isFavorited: false
    }
  }
]

export default followingFoodData