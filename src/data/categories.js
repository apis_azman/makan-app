const categoriesData = [
  {
    label: 'Following',
    background: 'https://www.bbcgoodfood.com/sites/default/files/editor_files/kerala-prawn-curry.jpg'
  },
  {
    label: 'Local',
    background: 'http://lh3.ggpht.com/K1BiBysQYph8tAbHsWVF4afo-mZ7vipZOWeYs91YONXIqmuMot_Cea4-a-NjE6JBA4DJ-rUtIRx0IX0QW5xupTAf=s300-c'
  }, 
  {
    label: 'Pizza',
    background: 'http://ghk.h-cdn.co/assets/cm/15/11/54fe019d32787-cheese-and-salad-pizza-de.jpg'
  }, 
  {
    label: 'Dessert',
    background: 'https://cache-graphicslib.viator.com/graphicslib/5277/SITours/dessert-walking-tour-in-new-york-city-cupcakes-cookies-and-gelato-in-new-york-city-118506.jpg'
  },
  {
    label: 'Sushi',
    background: 'https://b.zmtcdn.com/data/pictures/chains/0/16616870/cfdd4ea7003697120dee00af524dca1e.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A'
  }, 
  {
    label: 'Western',
    background: 'https://www.chessington.com/images/explore/food/offers/burger.jpg'
  }
]

export default categoriesData