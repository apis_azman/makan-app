const chatListData = [
    {
        name: 'Amy Farha',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
        food_title: 'Spaghetti Carbonara',
        last_message: 'Hello, I\'m interested in buying this',
        date:'Today, 8.20 pm',
        food_url: 'https://budgetbytes.com/wp-content/uploads/2016/05/Spaghetti-Carbonara-eat.jpg'
    },
    {
        name: 'Chris Jackson',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
        food_title: 'Nasi Lemak Antarabangsa',
        last_message: 'Do you COD?',
        date:'08/08/2017, 9.30 am',
        food_url:'https://i1.wp.com/angsarap.net/wp-content/uploads/2014/11/Nasi-Lemak-Wide.jpg'
    },
]

export default chatListData