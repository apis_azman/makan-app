import React from 'react'
import { 
  StyleSheet, 
  View, 
  ScrollView,
  TextInput, 
  TouchableOpacity, 
  Text, 
  ActivityIndicator 
} from 'react-native'
import PageBackground from '../../components/PageBackground'

import addMenuStyle from '../Food/styles/AddMenuStyle'
import styles from './styles/SignupFormStyle'

import { connect } from 'react-redux'
import { signupUser } from '../../redux/actions/accountAction'

const mapStateToProps = ({ accountReducer }) => {
  return {
    error: accountReducer.error,
    loading: accountReducer.loading
  }
}

const mapDispatchToProps = (dispatch) => ({
  signupUser: (payload) => dispatch(signupUser(payload))
})

class SignupPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: '',
      passwordConfirm: ''
    }
  }
  onValueChange(key, value) {
    this.setState({
      [key]: value
    })
  }
  onClickCreateAccount() {
    this.props.signupUser(this.state)
  }

  renderButton() {
    // if (this.props.loading) {
    //   return (
    //     <ActivityIndicator
    //       animating = {true}
    //       color = '#bc2b78'
    //       size = "large"
    //       style = {styles.activityIndicator}
    //     />
    //   )
    // }

    return (
      <TouchableOpacity onPress={() => this.onClickCreateAccount()} style={styles.buttonContainer}>
        <Text style={styles.buttonText}>
          CREATE ACCOUNT
        </Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.errorText}> { this.props.error } </Text>
        <TextInput 
          placeholder="name" 
          placeholderTextColor="rgba(255,255,255,0.7)" 
          returnKeyType="next"
          keyboardType="default" 
          underlineColorAndroid='transparent'
          style={styles.input}
          onChangeText={(value) => this.onValueChange('name', value)}
        />
        <TextInput 
          placeholder="email address" 
          placeholderTextColor="rgba(255,255,255,0.7)" 
          returnKeyType="next"  
          keyboardType="email-address" 
          underlineColorAndroid='transparent'
          style={styles.input}
          onChangeText={(value) => this.onValueChange('email', value)}
        />
        <TextInput 
          placeholder="password" 
          placeholderTextColor="rgba(255,255,255,0.7)" 
          secureTextEntry
          returnKeyType="next" 
          underlineColorAndroid='transparent'
          style={styles.input}
          onChangeText={(value) => this.onValueChange('password', value)}
        />
        <TextInput 
          placeholder="password confirmation" 
          placeholderTextColor="rgba(255,255,255,0.7)" 
          secureTextEntry 
          returnKeyType="go" 
          style={styles.input}
          onChangeText={(value) => this.onValueChange('passwordConfirm', value)}
        />
        { this.renderButton() }
        <View style={addMenuStyle.footerOverhead} />
      </ScrollView>
    )
  }
}

export default connect(mapStateToProps, { signupUser }) (SignupPage)