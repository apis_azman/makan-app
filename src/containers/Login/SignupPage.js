import React, { Component } from 'react'
import { StyleSheet, View, TextInput, TouchableOpacity, Text, KeyboardAvoidingView, Dimensions, Image } from 'react-native'
import PageBackground from '../../components/PageBackground'
import SignupForm from './SignupForm'
import styles from './styles/SignupPageStyle'
import { NavBarWithBack } from '../../navigation/NavBars'

import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialIcons'

var {height, width} = Dimensions.get('window');

class SignupPage extends Component {
    render() {
        return(
            <Image source={require('../../assets/images/splash-bg.png')} style={[styles.container, {width: width, height: height}]}>
                <View style={styles.backButton}>
                    <TouchableOpacity onPress={Actions.pop}>                
                            <Icon name='arrow-back' size={21} color='white' />
                    </TouchableOpacity>
                </View>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} source={require('../../assets/images/logo-white-transparent.png')} />
                    <Text style={styles.title}>Signup</Text>
                </View>
                <View style= {styles.formContainer}>
                    <SignupForm />
                </View>
            </Image>
        )
    }
}

export default SignupPage