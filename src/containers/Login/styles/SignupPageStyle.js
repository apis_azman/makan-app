import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    backButton: {
        padding: 10,
        backgroundColor: 'transparent',
        flexDirection: 'row', 
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    logo: {
        width: 100,
        height: 100
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        textAlign: 'center',
        opacity: 0.9,
        fontSize: 20,
    },
    formContainer: {
        flex: 3
    }
})