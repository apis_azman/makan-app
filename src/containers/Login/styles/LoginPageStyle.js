import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    logoContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        color: '#ffffff',
        marginTop: 10,
        opacity: 0.9,
        fontSize: 20,
    },
    title2: {
        fontSize: 70,
        color: '#ffffff',
        opacity: 0.9
    },
    title3: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#ffffff',
        opacity: 0.9,
        textAlign: 'center',
        marginBottom: 5
    },
    container: {
        flex: 1,
        padding: 10,
    },
    buttonWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    googleWrapper: {
        paddingRight: 3,
        flex: 1
    },
    emailWrapper: {
        paddingLeft: 3,
        flex: 1
    },
    logo: {
        width: 100,
        height: 100
    },
    title4: {
        fontSize: 12,
        color: '#ffffff',
        opacity: 0.9,
        textAlign: 'center',
    },
})