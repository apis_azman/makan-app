import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        height: 55,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 10,
        color: '#FFF',
        paddingHorizontal: 10,
        borderRadius: 5
    },
    buttonContainer: {
        backgroundColor: '#8e44ad',
        paddingVertical: 15,
        borderRadius: 5
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700'
    },
    signup: {
        color: 'grey',
        fontWeight: '500'
    },
    signupContainer: {
        // backgroundColor: '#8e44ad',
        paddingVertical: 15
    },
    errorText: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
})