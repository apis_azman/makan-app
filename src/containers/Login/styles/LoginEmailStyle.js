import { StyleSheet } from 'react-native'
import { colors }from '../../../themes'

export default StyleSheet.create({
     container: {
        flex: 1,
        padding: 10,
    }, 
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 100
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        textAlign: 'center',
        opacity: 0.9,
        fontSize: 20,
    },
    backButton: {
        padding: 10,
        backgroundColor: 'transparent',
        flexDirection: 'row', 
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    formContainer: {
        flex: 3
    }
})