import React, { Component } from 'react'
import { 
  Alert, 
  StyleSheet, 
  View, 
  ScrollView,
  TextInput, 
  TouchableOpacity, 
  Text, 
  ActivityIndicator 
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import styles from './styles/LoginFormStyle.js'
import { connect } from 'react-redux'
import { loginUser, signupCreate, dismissAlert } from '../../redux/actions/accountAction'

const mapStateToProps = ({ accountReducer }) => {
  return {
    error: accountReducer.error,
    loading: accountReducer.loading
  }
}

const mapDispatchToProps = (dispatch) => ({
  loginUser: (payload) => { dispatch(loginUser(payload)) },
  signupCreate: (email, password) => { dispatch(signupCreate(email, password)) },
  dismissAlert: () => { dispatch(dismissAlert()) }
})

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }
  onValueChange(key, value) {
    this.setState({
      [key]: value
    })
  }
  errorHandling(inputValues) {
    var result = {
      hasIssue: false, 
      message: ''
    }

    // email
    var email = inputValues.email
    if(email) {
      if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
        result.hasIssue = true
        result.message = 'Your email format is not matching the one that is universally used in this planet. Check again!'
        return result
      }
    }
    else {
      result.hasIssue = true
      result.message = 'What is your email?'
      return result
    }

    // password
    var password = inputValues.password
    if(!password) {
      result.hasIssue = true
      result.message = 'Password? Cmon man :/'
      return result
    }

    return result
  }
  onClickLogin() {
    var formValidate = this.errorHandling(this.state)

    if(!formValidate.hasIssue)
      this.props.loginUser(this.state)
    else
      alert(formValidate.message)
  }
  onClickSignup() {
    this.props.signupCreate(this.state.email, this.state.password);
  }
  renderButton() {
    return (
      <TouchableOpacity onPress={() => this.onClickLogin()} style={styles.buttonContainer}>
        <Text style={styles.buttonText}>
          LOGIN
        </Text>
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        {this.props.error ? Alert.alert(
          'Login Error',
          this.props.error,
          [
            {text: 'OK', onPress: () => this.props.dismissAlert()},
          ]
        ) : null}     
        <TextInput 
          placeholder="email address" 
          placeholderTextColor="rgba(255,255,255,0.7)"
          returnKeyType="next"
          keyboardType="email-address" 
          underlineColorAndroid='transparent'
          style={styles.input}
          onChangeText={(value) => this.onValueChange('email', value)}
        />
        <TextInput 
          placeholder="password" 
          placeholderTextColor="rgba(255,255,255,0.7)"
          secureTextEntry 
          returnKeyType="go"
          underlineColorAndroid='transparent'
          style={styles.input}
          onChangeText={(value) => this.onValueChange('password', value)}
        />
        { this.renderButton() }
        <TouchableOpacity onPress={() => this.onClickSignup()} style={styles.signupContainer}>
          <Text style={styles.signup}> Don't have an account yet? </Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

export default connect(mapStateToProps, { loginUser, signupCreate, dismissAlert })(LoginForm)