import React from 'react'
import { Text, View, Image, TouchableOpacity, TouchableHighlight, Button, StyleSheet, Dimensions } from 'react-native'
import Styles from './styles/LoginPageStyle'
import { GoogleSignin } from 'react-native-google-signin';
import PageBackground from '../../components/PageBackground'
import LoginButton from '../../components/Buttons/LoginButton'
import { Actions } from 'react-native-router-flux'
import { colors } from '../../themes'
import { connect } from 'react-redux'
import { doFacebookLogin, doGoogleLogin } from '../../redux/actions/accountAction';

// import FBSDK, { LoginManager, AccessToken, GraphRequest, infoRequest, GraphRequestManager } from 'react-native-fbsdk'

var {height, width} = Dimensions.get('window');

const mapStateToProps = state => {
    return {
        token: state.accountReducer.token
    }
}

// var responseInfoCallback = (error, result) => {
//     if (error) {
//       console.log(error)
//       alert('Error fetching data: ' + error.toString());
//     } else {
//       console.log(result)
//       alert('Success fetching data: ' + result.toString());
//     //  dispatch(signupUser)
//     }
//   }

// var requestGraphApi = (accessToken) => {
//     const infoRequest = new GraphRequest(
//     '/me',
//     {
//       accessToken: accessToken,
//       parameters: {
//         fields: {
//           string: 'email,name,first_name,middle_name,last_name'
//         }
//       }
//     },
//     responseInfoCallback
//   );

//   // Start the graph request.
//   new GraphRequestManager().addRequest(infoRequest).start();
// }

class LoginPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            user: null,
            isModalVisible: false
        }
    }    

    _fbLogin = () => {
        console.log('fb clicked');
        this.props.doFacebookLogin();
    }

    _googleLogin = () => {
        console.log('google login clicked')
        // GoogleSignin.signIn().then((user) => { 
        //     if (user){
        //         console.log('google sign user ' + user);
        //         console.log(user);
        //     } else {
        //         console.log("Cancelled google signin");
        //     }
        // })
        // .catch((err) => {
        //     console.log('WRONG SIGNIN', err);
        // }).done();

        this.props.doGoogleLogin();
    }
    
    _googleLogout = () => {
        GoogleSignin.signOut()
            .then((user) => {
                //do something
            })
            .catch((err) => {
                console.log('google sign out error: ', err)
            });
    }

    _onclick = () => {
        console.log("clicked");
        Actions.tabLoginEmail();
    }

    componentDidMount () {
        GoogleSignin.hasPlayServices({ autoResolve: true})
            .then( () => {
            GoogleSignin.configure({
                offlineAccess: false,
                // webClientId: ""
            })
            .then(()=> {
                GoogleSignin.currentUserAsync().then((user) => {
                console.log('Google USER in did mount: ', user);
                this.setState({ user: user})
                }).done();
            })
        })
        .catch((err) => {
            console.log("Play services error: Error code(",err.code +") Error Message(", err.message + (")"))
        })
    }

    render() {
        return (
            <Image source={require('../../assets/images/splash-bg.png')} style={[Styles.container, {width: width, height: height}]}>
                <View style={Styles.logoContainer}>
                    <Image source={require('../../assets/images/logo-white-transparent.png')} style={Styles.logo}/>
                    <Text style={Styles.title}> Find your Favourite </Text>
                    <Text style={Styles.title2}> FOOD </Text>
                </View>
                <View style={Styles.container}>
                    <Text style={Styles.title3}> SIGN UP <Text style={[Styles.title3,{fontWeight: 'normal'}]}>OR</Text> LOGIN </Text>
                    <LoginButton color={colors.fbButton} text="Login With Facebook" icon="facebook-official" onclick={ this._fbLogin.bind(this)}/>
                    <View style={Styles.buttonWrapper}>
                        { !this.state.user ? (
                            <View style={Styles.googleWrapper}>
                                <LoginButton color={colors.googleButton} text="Google" icon="google" onclick={ this._googleLogin.bind(this) }/>
                            </View> 
                        ) : (
                            <View style={Styles.googleWrapper}>
                                <LoginButton color={colors.googleButton} text="Sign Out" icon="google" onclick={ this._googleLogout.bind(this) }/>
                            </View> 
                        )}
                        <View style={Styles.emailWrapper}>
                            <LoginButton color={colors.emailButton} text="Email" icon="envelope" onclick={this._onclick.bind(this)}/>
                        </View>
                    </View>
                    <Text style={Styles.title4}> By signing up, you agree to our <Text style={{textDecorationLine: 'underline'}}>Terms of Service</Text> 
                    </Text>
                </View>
            </Image>
        )
    }
}

export default connect(mapStateToProps, { doFacebookLogin, doGoogleLogin })(LoginPage);
