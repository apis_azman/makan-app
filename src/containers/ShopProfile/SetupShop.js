import React from 'react'
import {
  View,
  Text,
  TextInput,
  Picker,
  ScrollView,
  ToastAndroid
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import { CheckBox, Button } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { setStoreDetails } from '../../redux/actions/accountAction'

import utilitiesServices from '../../services/utilitiesServices'
import storeServices from '../../services/storeServices'

import addMenuStyle from '../Food/styles/AddMenuStyle'
import foodDetailsStyle from '../Food/styles/FoodDetailsStyle'
import { colors } from '../../themes/'

import utilities from '../../utilities/utilities'

import ListSection from '../../components/ListSection'
import FullWidthButton from '../../components/Buttons/FullWidthButton'
import FormField from '../../components/Inputs/FormField'
import PhotoInput from '../../components/Inputs/PhotoInput'
import DropdownPicker from '../../components/Inputs/DropdownPicker'
import { NavBarWithBack } from '../../navigation/NavBars'

import states from '../../data/states'

// because prop for checkbox from react-native-elements can only except pure js object
const 
  checkBoxContainerStyle = {
    backgroundColor: colors.white,
    borderWidth: 0,
    paddingRight: 0,
    marginRight: 0
  }
var data, auth

const mapStateToProps = ({ accountReducer }) => ({
  user: accountReducer.user,
  auth: accountReducer.token
})

const mapDispatchToProps = (dispatch) => ({
  setStoreDetails: (store) => dispatch(setStoreDetails(store))
})

class SetupShop extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      storeId: '',

      name: '',
      user: this.props.user,
      photo: '',
      addr1: '',
      addr2: '',
      city: '',
      postcode: '',
      state: states[0],
      openingFrom: '',
      openingTo: '',
      description: '',
      geoLocation: {
        latd: '',
        longd: ''
      }
    }
  }
  componentWillMount() {
    let that = this

    // check if store id is passed, means now this is in edit mode
    if(this.props.storeId) {
      storeServices.getStoreByUserId(this.props.user.id, this.props.auth, function(res) {
        res = res.store
        that.setState({
          storeId: res._id,

          name: utilities.checkValue(res.name),
          photo: utilities.checkValue(res.imageUrl),
          photoId: utilities.checkValue(res.imageId),
          addr1: utilities.checkValue(res.address.address_1),
          addr2: utilities.checkValue(res.address.address_2),
          city: utilities.checkValue(res.address.city),
          postcode: utilities.checkValue(res.address.postcode),
          state: utilities.checkValue(res.address.state),
          openingFrom: utilities.checkValue(res.operatingModel.operationHours.from),
          openingTo: utilities.checkValue(res.operatingModel.operationHours.to),
          description: utilities.checkValue(res.description),
          geoLocation: {
            latd: utilities.checkValue(res.geoLocation.latd),
            longd: utilities.checkValue(res.geoLocation.longd)
          }
        })
      })
    }
  }

  onChangeValue(key, value) {
    this.setState({
      [key]: value
    })
  }
  onSubmit = () => {
    props = this.props
    data = this.state
    auth = this.props.auth
    userId = this.props.user.id

    if(this.props.action == 'edit') {
      utilitiesServices.updateUploadFile(this.state.photo, this.state.photoId, auth, function(res) {
        data.photo = res ? res.file.id : data.photoId
        storeServices.updateStore(data, auth, function(res) {
          storeServices.getStoreByUserId(userId, auth, function(res) {
            props.setStoreDetails(res.store)
            Actions.tabHome({ index: 3 })
          })
        })
      })
    } else {
      utilitiesServices.uploadFile(this.state.photo, auth, function(res) {
        data.photo = res ? res.file.id : ''
        storeServices.setupStore(data, auth, function(res) {
          storeServices.getStoreByUserId(userId, auth, function(res) {
            props.setStoreDetails(res.store)
            ToastAndroid.show('Congratulation! now you have KITCHEN', ToastAndroid.SHORT)
            Actions.tabHome({ index: 1 })
          })
        })
      })
    }
    
  }
  renderTimePicker(key) {
    return (
      <DatePicker 
        mode='time'
        placeholder='Select time'
        customStyles={{ dateInput:{ alignItems: 'flex-start', borderWidth: 0 } }} 
        date={this.state[key]} 
        is24hour={false}
        confirmBtnText='Ok' 
        cancelBtnText='Cancel'
        showIcon={false}
        onDateChange={(time) => this.onChangeValue(key, time)}
      />
    )
  }
  renderTextInput(placeholder, key) {
    return (
      <TextInput style={addMenuStyle.textInput} underlineColorAndroid='rgba(0,0,0,0)' placeholder={placeholder} value={this.state[key]} onChangeText={(itemValue) => this.onChangeValue(key, itemValue)}></TextInput>
    )
  }

  handlePickLocation = (data) => {
    this.setState({ geoLocation: data })
  }

  render() {
    let labels = { pageTitle: '', buttonLabel: ''}
    if(this.props.action == 'edit') {
      labels.pageTitle = 'Edit Shop'
      labels.buttonLabel = 'SAVE'
    }
    else {
      labels.pageTitle = 'Setup shop'
      labels.buttonLabel = 'CREATE SHOP!'
    }

    return (
      <View>
        <NavBarWithBack pageTitle={ labels.pageTitle } />
        <ScrollView>
          <View style={addMenuStyle.photoSection}>
            <PhotoInput inputIcon='store' photoLabel='Shop Photo' value={this.state.photo} returnImg={(img) => this.onChangeValue('photo', img)}/>
          </View>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Shop Details</Text>
            <FormField label='Name' inputComponent={this.renderTextInput('Name your shop', 'name')} />
          </ListSection>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Address</Text>
            <FormField label='Line 1' inputComponent={this.renderTextInput('Address line 1', 'addr1')} />
            <FormField label='Line 2' inputComponent={this.renderTextInput('Address line 2', 'addr2')} />
            <FormField label='City' inputComponent={this.renderTextInput('City', 'city')} />
            <FormField label='Postcode' inputComponent={
              <TextInput style={addMenuStyle.textInput} underlineColorAndroid='rgba(0,0,0,0)' keyboardType="numeric" placeholder="Postcode" value={this.state.postcode} onChangeText={(itemValue) => this.onChangeValue('postcode', itemValue)}></TextInput>
            } />
            <FormField 
              label='State'
              inputComponent={
                <DropdownPicker options={states} value={this.state.state} onChangeValue={(itemValue) => this.onChangeValue('state', itemValue)} />
              }
            />
            <FormField label='Location' inputComponent={<Button rounded onPress={() => Actions.pickLocation({ action: this.handlePickLocation })} title="Pick Location" />} />
          </ListSection>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Opening Hours</Text>
            <FormField label='From' inputComponent={
              this.renderTimePicker('openingFrom')
            } />
            <FormField label='To' inputComponent={
              this.renderTimePicker('openingTo')
            } />
          </ListSection>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Others</Text>
            <FormField label='Description' inputComponent={this.renderTextInput('Tell more about your shop', 'description')} />
          </ListSection>
          <FullWidthButton label={ labels.buttonLabel } action={this.onSubmit} />
          <View style={addMenuStyle.footerOverhead} />
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps, { setStoreDetails })(SetupShop)
