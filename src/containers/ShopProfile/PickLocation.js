import React from 'react'
import { NavBarWithBack } from '../../navigation/NavBars'
import { ScrollView, Text, View, Dimensions, StyleSheet, ActivityIndicator, ToastAndroid } from 'react-native'
import MapView, { Marker, ProviderPropType } from 'react-native-maps'
import CustomMarker from '../../components/CustomMarker'
import { Button } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'

const { height, width } = Dimensions.get('window');

const ASPECT_RATIO = width /height;
const HALFHEIGHT = width/1.5;
const LATITUDE = 2.917969;
const LONGITUDE = 101.651812;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
// const SPACE = 0.01; 

function log(eventName, e) {
    console.log(eventName, e.nativeEvent);
}

export default class PickLocation extends React.Component {
    constructor(props){
        super(props)

        map = null;
        
        this.state = {
            mapLoaded: false,
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            a: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
            },
            markerPosition: {
                y: null,
                x: null
            },
            markerCoordinate: {
                latd: null,
                longd: null
            }
        };
        this.onRegionChange = this.onRegionChange.bind(this);
    }

    componentDidMount() {
        this.getCurrentPosition();
        this.setState({ mapLoaded: true});
    }

    onRegionChange(region) {
        this.setState({ region })
    }

    onRegionChangeComplete = (region) => {
        this.setState({ region })
    }

    getCurrentPosition() {
        try {
        navigator.geolocation.getCurrentPosition(
            (position) => {
            const region = {
                latitude: position.coords.latitude  - 0.0007,
                longitude: position.coords.longitude + 0.006,
                latitudeDelta: 0.00922*1.5,
                longitudeDelta: 0.00421*1.5,
            };

            const newA = {
                latitude: position.coords.latitude - 0.0007 ,
                longitude: position.coords.longitude + 0.006
            }

            const newB = {
                latd: newA.latitude,
                long: newA.longitude
            }

            this.setRegion(region);
            this.setState({ a: newA });
            this.setState({markerCoordinate: newB});
            },
            (error) => {
            if (error.code) {
                console.log('Error getting user loakasi')
                }
            });
        } catch(e) {
         alert(e.message || "");
        }
    };

    setRegion(region) {
        if(this.state.mapLoaded) {
            setTimeout(() => this._map.animateToRegion(region), 1000);
        }
        //this.setState({ region });
    }

    onDragEnded(eventName, e) {
        console.log(eventName, e.nativeEvent);
        console.log(e.nativeEvent);

        const newMarkerPosition = {
            y: e.nativeEvent.position.y ,
            x: e.nativeEvent.position.x
        }

        const newMarkerCoordinate = {
            latd: e.nativeEvent.coordinate.latitude,
            longd: e.nativeEvent.coordinate.longitude
        }
        this.setState({ markerPosition: newMarkerPosition });
        this.setState({ markerCoordinate: newMarkerCoordinate });

    }

    onClickConfirm(location) {
        this.props.action(location);
        Actions.pop();
        ToastAndroid.show('Location saved!', ToastAndroid.SHORT)
    }
    
    render(){

        if(!this.state.mapLoaded) {
            return (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <ActivityIndicator size="large" />
                </View>
            )
        }

        return (
            <View style={styles.container}>
                <NavBarWithBack pageTitle='Pick Location' />
                <View style={styles.wrapper}>

                    <MapView
                        provider={this.props.provider}
                        ref={ component => { this._map = component }}
                        style={styles.map} 
                        // initialRegion={this.state.region}
                        showsUserLocation={false}
                        followsUserLocation={false}
                        // region={this.state.region}
                        // onRegionChange={this.onRegionChange}>
                        onRegionChangeComplete={this.onRegionChangeComplete}
                        >
                        <Marker 
                            coordinate={this.state.a}
                            onSelect={(e) => log('onSelect', e)}
                            onDrag={(e) => log('onDrag', e)}
                            onDragStart={(e) => log('onDragStart', e)}
                            onDragEnd={(e) => this.onDragEnded('onDragEnd', e)}
                            onPress={(e) => log('onPress', e)}
                            draggable
                            >
                            <CustomMarker />
                        </Marker> 
                    </MapView>
                    <View style={styles.instructionWrapper}>
                        <Text style={styles.instruction}>Hold and drag marker</Text>
                    </View>
                </View>
                
                <View style={styles.buttonContainer}>
                    <Button rounded buttonStyle={{opacity: 0.8}} onPress={() => this.onClickConfirm(this.state.markerCoordinate)} title="Confirm" />
                </View>
            </View>
        )
    }
}

PickLocation.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    map: {
        // ...StyleSheet.absoluteFillObject,
        width: width,
        // height: HALFHEIGHT,
        height: height,
        position: 'absolute'
    },
    container: {
        // ...StyleSheet.absoluteFillObject,
        
        flex: 1
    },
    wrapper: {
        flex:1,
        // justifyContent: 'flex-end',
        // alignItems: 'center',
    },
    buttonContainer: {
        marginBottom: 10,
    },
    instructionWrapper: {
        alignItems: 'center',
        marginTop: 10,
    },
    instruction: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 15
    }
})