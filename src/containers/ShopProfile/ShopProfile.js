import React from 'react'
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'

import PageBackground from '../../components/PageBackground'
import ProfileImage from '../../components/ProfileImage'
import { NavBarWithBack } from '../../navigation/NavBars'

import profileStyles from '../Landing/styles/ProfileStyle'

import storeServices from '../../services/storeServices'

const mapStateToProps = ({ accountReducer }) => ({
  auth: accountReducer.token
})

class ShopProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shopProfile: ''
    }
  }
  componentWillMount() {
    var that = this
    storeServices.getStoreByStoreId(this.props.storeId, this.props.auth, function(data) {
      that.setState({ shopProfile: data.store })
    })
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <NavBarWithBack pageTitle='Shop Profile' />
        <ScrollView style={profileStyles.container}>
          <PageBackground>
            <View style={profileStyles.profileContainer}>
              <ProfileImage dimension={120} uri={''} type='store' />
              <Text style={profileStyles.profileName}>{ this.state.shopProfile.name }</Text>
            </View>
          </PageBackground>
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps)(ShopProfile)