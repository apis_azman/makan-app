import React from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ScrollView
} from 'react-native'

import addMenuStyle from '../Food/styles/AddMenuStyle'
import chatPageStyle from '../Chat/styles/ChatPageStyle'
import foodDetailsStyle from '../Food/styles/FoodDetailsStyle'
import { colors, fonts } from '../../themes'

import ListSection from '../../components/ListSection'
import FormField from '../../components/Inputs/FormField'
import { NavBarWithBack } from '../../navigation/NavBars'

// temp
var menu = {
  menuImage: 'https://pbs.twimg.com/profile_images/2395928085/tio3omcu9zth8ifroz05_400x400.jpeg',
  menuName: 'Hansome Salad',
  menuPrice: 'RM 49.90'
}

export default class OrderFood extends React.Component {
  renderTextInput(placeholder) {
    return (
      <TextInput style={addMenuStyle.textInput} underlineColorAndroid='rgba(0,0,0,0)' placeholder={placeholder}></TextInput>
    )
  }
  render() {
    return (
      <View style={chatPageStyle.container}>
        <NavBarWithBack />
        <MenuBanner menuId='someFoodId' menuImage={menu.menuImage} menuName={menu.menuImage} menuPrice={menu.menuPrice}/>
        <ScrollView>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Order Form</Text>
            <FormField label='Pickup Options' inputComponent={this.renderTextInput('Self Pickup')} />
            <FormField label='Quantity' inputComponent={this.renderTextInput()} />
            <FormField label='Coupon code' inputComponent={this.renderTextInput()} />
            <FormField label='Grand Total' inputComponent={this.renderTextInput()} />
            <FormField label='6% GST' inputComponent={this.renderTextInput()} />
            <FormField label='Total' inputComponent={this.renderTextInput()} />
          </ListSection>
          <View style={[foodDetailsStyle.boxContainer, { marginTop: 25, height: 70 }]}>
            <TouchableOpacity style={foodDetailsStyle.chatButton} onPress={() => Actions.chatPage()}>
              <IconAwesome name='comment-o' size={20} color={colors.accent2}>
                <Text style={foodDetailsStyle.boldText}>  CHAT</Text>
              </IconAwesome>
            </TouchableOpacity>
            <TouchableOpacity style={foodDetailsStyle.orderButton}>
              <IconAwesome name='basket' size={20} color={colors.white}>
                <Text style={foodDetailsStyle.boldText}>  CHECKOUT</Text>
              </IconAwesome>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}