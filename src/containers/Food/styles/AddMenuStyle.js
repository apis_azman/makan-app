import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes'

export default StyleSheet.create({
  // for some of the fields that is not using FormField component
  formContainer: {
    paddingTop: 10, 
    borderBottomWidth: 1, 
    borderColor: colors.lightGrey
  },
  photoSection: { 
    padding: 10, 
    height: 200, 
    backgroundColor: colors.lightGrey, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  photoBox: { 
    width: 130, 
    height: 130, 
    marginTop: 20,
    backgroundColor: 'white', 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  photoLabel: { 
    height: 40, 
    width: 130, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  textInput: {
    fontStyle: 'italic'
  },
  // fix for last form section covered by the android nav bar
  footerOverhead : {
    height: 50
  }
})