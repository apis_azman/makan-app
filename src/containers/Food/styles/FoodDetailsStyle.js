import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes/'

export default StyleSheet.create({
  imageStyle: {
    position: 'absolute', 
    top: 0, 
    right: 0, 
    bottom: 0, 
    left: 0
  },
  foodDetailsTopContainer: { 
    flex: 2, 
    flexDirection: 'column' 
  },
  foodDetailsTop: { 
    backgroundColor: colors.transparent, 
    flexDirection: 'row' 
  },
  foodDetailsTopItems: { 
    flex: 2, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  foodDetailsTopItemText: { 
    marginTop: 3 
  },
  foodDetailsContainer: { 
    flex: 3, 
    backgroundColor: colors.white,
    paddingTop: 10, 
    paddingRight: 10, 
    paddingLeft: 10, 
    paddingBottom: 20 
  },
  foodTitleContainer: { 
    flexDirection: 'row', 
    justifyContent: 'space-between' 
  },
  iconContainer: { 
    flexDirection: 'row', 
    alignItems: 'center' 
  },
  foodName: {
    color: colors.black,
    fontSize: fonts.size.h4,
    fontWeight: 'bold'
  },
  nonHalalLabel: { 
    fontSize: fonts.size.medium, 
    color: colors.accents2, 
    fontWeight: '500', 
    fontStyle: 'italic' 
  },
  foodPriceContainer: { 
    flexDirection: 'row',
    alignItems: 'center' 
  },
  foodPrice: { 
    fontSize: fonts.size.h4, 
    color: colors.accent2 
  },
  boxContainer: { 
    flexDirection: 'row',
    alignItems: 'center', 
    marginTop: 10
  },
  starRating: { 
    fontSize: 20, 
    color: 'yellow' 
  },
  ratingCount: { 
    fontSize: 16, 
    marginLeft: 10, 
    color: colors.black 
  },
  actionButtonsContainer: { 
    flex: 2, 
    backgroundColor: colors.accent2, 
    paddingTop: 3, 
    flexDirection: 'row' 
  },
  chatButton: { 
    flex: 1,
    height: 70,
    borderWidth: 2,
    borderColor: colors.accent2,
    backgroundColor: colors.white, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  orderButton: { 
    flex: 1, 
    height: 70,
    backgroundColor: colors.accent2, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  deliverySection: { 
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center', 
    height: 85
  },
  deliveryLabel: { 
    fontSize: fonts.size.medium, 
    color: colors.black, 
    marginTop: 5 
  },
  boldText: {
    fontWeight: '500'
  },
  sectionTitle: { 
    color: colors.black, 
    fontSize: fonts.size.h5, 
    fontWeight: '500' 
  },
  shopImageContainer: { 
    flex: 2, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  shopDetailsContainer: { 
    flex: 4, 
    alignItems: 'flex-start', 
    flexDirection: 'column' 
  },
  followButtonContainer: { 
    flex: 2, 
    justifyContent: 'center', 
    alignItems: 'flex-start' 
  },
  reviewMessage: { 
    marginTop: 10, 
    fontSize: fonts.size.regular, 
    textAlign: 'center' 
  }
})