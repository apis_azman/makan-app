import React from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  Dimensions
} from 'react-native'
import Image from 'react-native-image-progress'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconAwesome from 'react-native-vector-icons/FontAwesome'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import { Actions } from 'react-native-router-flux'

import { colors, fonts } from '../../themes'
import FoodStyles from '../../components/styles/FoodStyle'
import styles from './styles/FoodDetailsStyle'

import utilities from '../../utilities/utilities'
import { NavBarWithBackandOptions } from '../../navigation/NavBars'
import ListSection from '../../components/ListSection'
import ProfileImage from '../../components/ProfileImage'
import TextButton from '../../components/Buttons/TextButton'
import Review from '../../components/Review'

// temp
const foodDetails = {
  name: 'Homemade Pancakes',
  price: '3.90',
  foodImage: 'http://ghk.h-cdn.co/assets/16/38/980x490/landscape-1474822198-how-to-make-pancakes.jpg',
  nonHalal: true,
  rating: 5,
  ratingCount: 25,
  location: 'Cyberjaya, Selangor',
  sellerName: 'Shell In!',
  sellerImage: 'http://ghk.h-cdn.co/assets/16/38/980x490/landscape-1474822198-how-to-make-pancakes.jpg',
  sellerRating: 4,
  sellerRatingCount: 20,
  reviews: [
    {
      title: 'Sodap!',
      rating: 5,
      user: 'Aisyah LV',
      date: '2 July 2016',
      message: 'Padu beb'
    },
    {
      title: 'Mantop',
      rating: 5,
      user: 'James Lulu',
      date: '3 March 2016',
      message: 'Aku dah lama mengidam duk cari dekat sana sini semua tak ada. Selepas menggunakan app makan barulah ak berjumpa pancake pilihan hati. Terima kasih!'
    }
  ]
}

const
  windowHeight = Dimensions.get("window").height,
  headerHeight = windowHeight * 0.6,
  imgDotMarginBottom = headerHeight * 0.8

export default class FoodDetails extends React.Component {
  onOrderClick() {
    Actions.orderFood()
  }
  renderFoodRating() {
    if(foodDetails.ratingCount <= 0)
      return (
        <TouchableOpacity>
          <Text>Be the first to <Text style={{ color: colors.accent2 }}>write a review</Text></Text>
        </TouchableOpacity>
      )
    else
      return (
        <View style={[styles.boxContainer, { marginTop: 5 }]}>
          { utilities.renderRatingStar(foodDetails.rating, styles.starRating) }
          <Text style={styles.ratingCount}>({ foodDetails.ratingCount })</Text>
        </View>
      )
  }
  renderTopSection() {
    return (
      <View style={[ FoodStyles.foodDetailsContainer, styles.foodDetailsContainer]}>
        <View style={styles.foodTitleContainer}>
          <Text style={styles.foodName}>{ foodDetails.name }</Text>
          <View style={styles.iconContainer}>
            {/* commented 16/8/2017 for Alpha Release
            <TouchableOpacity style={{ marginRight: 20 }}>
              <Icon name='share' size={25} color={colors.accent2} />
            </TouchableOpacity>
            */}
            <TouchableOpacity>
              <Icon name='heart-outline' size={25} color={colors.accent2} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.foodPriceContainer}>
          <Text style={styles.foodPrice}>RM { foodDetails.price }</Text>
          {/* commented 16/8/2017 for Alpha Release
          <Text style={[FoodStyles.foodOriginalPrice, { fontSize: 18, color: colors.lightGrey }]}>RM 5.00</Text>
          <Text style={[styles.foodPrice, { marginLeft: 10 }]}>-22%</Text>
          */}
        </View>
        { this.renderFoodRating() }
        {
          foodDetails.nonHalal ? (
            <View style={styles.boxContainer}>
              <IconAwesome name='exclamation-triangle' size={15} color={colors.accent2}>
                <Text style={styles.nonHalalLabel}>  Contains NON-HALAL ingredient</Text>
              </IconAwesome>
            </View>
          ) : ''
        }
        <View style={styles.boxContainer}>
          <IconAwesome name='clock-o' size={15} color={colors.accent2}>
            <Text style={{ fontSize: fonts.size.medium, color: colors.black }}>  Ready in 24 hours</Text>
          </IconAwesome>
        </View>
        <View style={[styles.boxContainer, { marginTop: 25, height: 70 }]}>
          <TouchableOpacity style={styles.chatButton} onPress={() => Actions.chatPage()}>
            <IconAwesome name='comment-o' size={20} color={colors.accent2}>
              <Text style={styles.boldText}>  CHAT</Text>
            </IconAwesome>
          </TouchableOpacity>
          <TouchableOpacity style={styles.orderButton} onPress={() => this.onOrderClick()}>
            <IconAwesome name='cutlery' size={20} color={colors.white}>
              <Text style={styles.boldText}>  ORDER</Text>
            </IconAwesome>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  renderRatingSection() {
    if(foodDetails.reviews.length <= 0)
      return (
        <View>
          <Text style={styles.reviewMessage}>This product haven't been reviewed yet. Be the first to review it!</Text>
        </View>
      )
    else
      return foodDetails.reviews.map((review, i) => {
        return <Review key={i} title={review.title} rating={review.rating} date={review.date} user={review.user} message={review.message}/>
      })
  }
  render() {
    return (
      <ParallaxScrollView
        backgroundColor={colors.main}
        parallaxHeaderHeight={headerHeight}
        renderForeground={() => (
          <Image source={{uri: foodDetails.foodImage}} resizeMode='cover' style={[styles.imageStyle, { height: headerHeight }]}>
            <NavBarWithBackandOptions backgroundColor={colors.transparent} iconColor={colors.black} optionsOnClick={() => alert('helloWorld')}/>
            {/* commented 16/8/2017 for Alpha Release
            <View style={styles.foodDetailsTopContainer}>
              <View style={styles.foodDetailsTop}>
                <View style={{ flex: 1 }} />
                <View style={styles.foodDetailsTopItems}>
                  <Icon name='watch-later' size={30} color={colors.accent2} />
                  <Text style={styles.foodDetailsTopItemText}>All day</Text>
                </View>
                <View style={styles.foodDetailsTopItems}>
                  <Icon name='person' size={30} color={colors.accent2} />
                  <Text style={styles.foodDetailsTopItemText}>2 Persons</Text>
                </View>
                <View style={styles.foodDetailsTopItems}>
                  <Icon name='alarm' size={30} color={colors.accent2} />
                  <Text style={styles.foodDetailsTopItemText}>30 minutes</Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>
            </View>
          */}
          </Image>
        )}
      >
        <View>
          { this.renderTopSection() }
          <ListSection>
            <Text style={styles.sectionTitle}>Delivery & Pickup Options</Text>
            <View style={styles.deliverySection}>
              <View style={styles.boxContainer, { flex: 1, alignItems: 'center' }}>
                <Icon name='truck' size={35} color={colors.accent2}/>
                <Text style={styles.deliveryLabel}>Delivery</Text>
              </View>
              <View style={styles.boxContainer, { flex: 1, alignItems: 'center' }}>
                <Icon name='store' size={35} color={colors.accent2}/>
                <Text style={styles.deliveryLabel}>Self Pickup</Text>
              </View>
            </View>
          </ListSection>
          <ListSection>
            <Text style={styles.sectionTitle}>Sold & Fulfilled by</Text>
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.boxContainer, styles.shopImageContainer]}>
                <ProfileImage dimension={80} uri={foodDetails.sellerImage} />
              </View>
              <View style={[styles.boxContainer, styles.shopDetailsContainer]}>
                <Text style={styles.foodPrice}>{ foodDetails.sellerName }</Text>
                <View style={[styles.boxContainer, { marginTop: 5 }]}>
                  { utilities.renderRatingStar(foodDetails.sellerRating, styles.starRating) }
                  <Text style={styles.ratingCount}>({ foodDetails.sellerRatingCount })</Text>
                </View>
              </View>
              <View style={[styles.boxContainer, styles.followButtonContainer]}>
                <TextButton label="Follow" action={() => alert('follow')}/>
              </View>
            </View>
          </ListSection>
          <ListSection>
            <Text style={styles.sectionTitle}>Reviews</Text>
            { this.renderRatingSection() }
          </ListSection>
        </View>
      </ParallaxScrollView>
    )
  }
}