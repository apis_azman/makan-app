import React from 'react'
import { ScrollView, View } from 'react-native'
import { connect } from 'react-redux'

import styles from '../Landing/styles/CategoriesStyle'

import { MainNavBarWithBack } from '../../navigation/NavBars'
import Food from '../../components/Food'
import FilterBar from '../../components/Inputs/FilterBar'

import menuServices from '../../services/menuServices'

const mapStateToProps = ({ accountReducer }) => ({
  auth: accountReducer.token,

  // temp
  category: 'all'
})

class FoodCatalog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      'menu': []
    }
  }
  componentWillMount() {
    var that = this

    if(this.props.category == 'all')
    menuServices.getMenu(this.props.auth, function(menu) {
      that.setState({ 'menu': menu.menus })
    })
    // else call api get menu by category
  }
  renderFoodRow() {
    var counter = 0, foodData = this.state.menu, rowFoods = []
    return foodData.map((food, i) => {
      if((foodData.length % 2 != 0) && (i == (foodData.length - 1))) {
        rowFoods[0] = food
        rowFoods[1] = 'empty'
        counter = 2
      }
      else {
        rowFoods[counter] = food
        counter++
      }

      if(counter == 2) {
        counter = 0
        return (
          <View key={i} style={styles.categoriesRow}>
            { this.renderFood(rowFoods) }
          </View>
        )
      }
    })
  }
  renderFood(rowFoods) {
    if(food.length == 0)
      return (
        // fix this tak keluar
        <Text style={{ flex: 1, paddingTop: 50, color: 'red' }}>Meeh.. don't have food</Text>
      )
    else
    return rowFoods.map((food, i) => {
      return (
        <Food key={i} food={food} showShopLabel />
      )
    })
  }
  render() {
    return (
      <View>
        <MainNavBarWithBack pageTitle={this.props.page} />
        {/* <FilterBar /> */}
        <ScrollView style={styles.container}>
          { this.renderFoodRow() }
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps, {})(FoodCatalog)