import React from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ScrollView,
  ToastAndroid
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { CheckBox } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import Collapsible from 'react-native-collapsible'
import DatePicker from 'react-native-datepicker'

import { setStoreDetails } from '../../redux/actions/accountAction'

import utilitiesServices from '../../services/utilitiesServices'
import menuServices from '../../services/menuServices'
import storeServices from '../../services/storeServices'

import style from './styles/AddMenuStyle'
import foodDetailsStyle from './styles/FoodDetailsStyle'
import { colors } from '../../themes/'

import ListSection from '../../components/ListSection'
import FullWidthButton from '../../components/Buttons/FullWidthButton'
import FormField from '../../components/Inputs/FormField'
import PhotoInput from '../../components/Inputs/PhotoInput'
import { NavBarWithBack } from '../../navigation/NavBars'
import ImagePicker from 'react-native-image-picker'
import DropdownPicker from '../../components/Inputs/DropdownPicker'

// because prop for checkbox from react-native-elements can only except pure js object
const 
  checkBoxContainerStyle = {
    backgroundColor: colors.white,
    borderWidth: 0,
    paddingRight: 0,
    marginRight: 0
  },
  preparationTime = ['5 minutes', '10 minutes', '15 minutes', '30 minutes', '45 minutes', '1 hour' ]
var data, auth

const mapStateToProps = ({ accountReducer }) => ({
  user: accountReducer.user,
  store: accountReducer.store,
  auth: accountReducer.token
})

const mapDispatchToProps = (dispatch) => ({
  setStoreDetails: (store) => dispatch(setStoreDetails(store))
})

class AddMenu extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      courses: [],
      categories: [],

      store: this.props.store, // get from redux
      title: '',
      photo: '',
      course: '',
      category: '',
      tags: '',
      halal: false,
      delivery: false,
      price: '',
      prepTime: preparationTime[0],
      availableDays: {
        mo: false,
        tu: false,
        we: false,
        th: false,
        fr: false,
        sa: false,
        su: false
      },
      allTime: false,
      availableFrom: null,
      availableTo: null
    }
  }
  // get course and category when mounting
  componentWillMount() {
    auth = this.props.auth
    that = this

    menuServices.getCourses(auth, function(courses) {
      var courseList = []
      for(var i=0; i<courses.courses.length; i++)
        courseList.push(courses.courses[i].label)
      that.setState({ courses: courseList })
      that.setState({ course: courseList[0] })
      menuServices.getCategories(auth, function(categories) {
        var categoryList = []
        for(var i=0; i<categories.categories.length; i++)
          categoryList.push(categories.categories[i].label)
        that.setState({ categories: categoryList })
        that.setState({ category: categoryList[0] })
      })
    })
  }
  onChangeValue(key, value) {
    this.setState({
      [key]: value
    })
  }
  onChangeAvailableDays(key, value) {
    availableDays = this.state.availableDays
    availableDays[key] = value
    this.setState({
      availableDays: availableDays
    })
  }
  onSubmit = () => {
    props = this.props
    data = this.state
    auth = this.props.auth

    userId = this.props.user.id
    storeId = this.props.store._id

    utilitiesServices.uploadFile(this.state.photo, auth, function(res) {
      data.photo = res ? res.file.id : ''
      menuServices.createMenu(data, auth, function(res) {
        storeServices.getStoreByUserId(userId, auth, function(res) {
          props.setStoreDetails(res.store)
          ToastAndroid.show('Menu added!', ToastAndroid.SHORT)
          Actions.tabHome({ index: 3 })
        })
      })
    })
  }
  renderTextInput(placeholder, key) {
    return (
      <TextInput style={style.textInput} underlineColorAndroid='rgba(0,0,0,0)' placeholder={placeholder} onChangeText={(itemValue) => this.onChangeValue(key, itemValue)}></TextInput>
    )
  }
  renderAvailableDaysCheckbox(days) {
    that = this
    return days.map((day, i) => {
      title = day.charAt(0).toUpperCase() + day.charAt(1)
      return (<CheckBox key={i} containerStyle={checkBoxContainerStyle} title={title} onPress={() => that.onChangeAvailableDays(day, !that.state.availableDays[day])} checked={that.state.availableDays[day]}/>)
    })
  }
  renderTimePicker(key) {
    return (
      <DatePicker 
        mode='time'
        placeholder='Select time'
        customStyles={{ dateInput:{ alignItems: 'flex-start', borderWidth: 0 } }} 
        date={this.state[key]} 
        is24hour={false}
        confirmBtnText='Ok' 
        cancelBtnText='Cancel'
        showIcon={false}
        onDateChange={(time) => this.onChangeValue(key, time)}
      />
    )
  }
  render() {
    return (
      <View>
        <NavBarWithBack pageTitle='Add Menu' />
        <ScrollView>
          <View style={style.photoSection}>
            <PhotoInput inputIcon='camera' photoLabel='Cover Photo' returnImg={(img) => this.onChangeValue('photo', img)}/>
          </View>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Menu Details</Text>
            <FormField label='Title' inputComponent={this.renderTextInput('Name your dish', 'title')} />
            <FormField 
              label='Course' 
              inputComponent={
                <DropdownPicker options={this.state.courses} value={this.state.course} onChangeValue={(itemValue) => this.onChangeValue('course', itemValue)} />
              } />
            <FormField 
              label='Category' 
              inputComponent={
                <DropdownPicker options={this.state.categories} value={this.state.category} onChangeValue={(itemValue) => this.onChangeValue('category', itemValue)} />
            } />
            <FormField label='Tags' inputComponent={this.renderTextInput('3 words to describe your dish', 'tags')} />
            <FormField label='Any non-halal ingredients used?' isCheckBox inputComponent={<CheckBox containerStyle={checkBoxContainerStyle} title='' iconRight onPress={() => this.onChangeValue('halal', !this.state.halal)} checked={this.state.halal}/>} />
            <FormField label='Price' inputComponent={
              <TextInput style={style.textInput} underlineColorAndroid='rgba(0,0,0,0)' keyboardType="numeric" placeholder="Set your price in RM" onChangeText={(itemValue) => this.onChangeValue('price', itemValue)}></TextInput>
            } />
          </ListSection>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Delivery & Pickup Options</Text>
            <FormField label='Do you do delivery?' isCheckBox inputComponent={<CheckBox containerStyle={checkBoxContainerStyle} title='' iconRight onPress={() => this.onChangeValue('delivery', !this.state.delivery)} checked={this.state.delivery}/>} />
            <FormField 
              label='Preparation Time' 
              inputComponent={
                <DropdownPicker options={preparationTime} value={this.state.prepTime} onChangeValue={(itemValue) => this.onChangeValue('prepTime', itemValue)} />
            } />
            <View style={style.formContainer}>
              <Text>Available Days</Text>
              <View style={{ flexDirection: 'row' }}>
                { this.renderAvailableDaysCheckbox(['mo', 'tu', 'we', 'th']) }
              </View>
              <View style={{ flexDirection: 'row' }}>
                { this.renderAvailableDaysCheckbox(['fr', 'sa', 'su']) }
              </View>
            </View>
            <View style={style.formContainer}>
              <Text>Available Hours</Text>
              <FormField label='All time?' isCheckBox inputComponent={<CheckBox containerStyle={checkBoxContainerStyle} title='' iconRight onPress={() => this.onChangeValue('allTime', !this.state.allTime)} checked={this.state.allTime}/>} />
              <Collapsible collapsed={this.state.allTime}>
                <FormField label='Available from' inputComponent={
                  this.renderTimePicker('availableFrom')
                } />
                <FormField label='Available to' inputComponent={
                  this.renderTimePicker('availableTo')
                } />
              </Collapsible>
            </View>
          </ListSection>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>Others</Text>
            <FormField label='Description' inputComponent={this.renderTextInput('Details about your dish', 'description')} />
          </ListSection>
          <FullWidthButton label='CREATE MENU' action={this.onSubmit} />
          <View style={style.footerOverhead} />
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps, { setStoreDetails })(AddMenu)
