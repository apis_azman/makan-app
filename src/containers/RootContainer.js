import React from 'react'
import { View, StatusBar, Text } from 'react-native'
import SpinnerOverlay from 'react-native-loading-spinner-overlay'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import NavigationRouter from '../navigation/NavigationRouter'
import AlertModal from '../components/Modals/AlertModal'

import { colors } from '../themes/'
import styles from './styles/RootContainerStyle'

const mapStateToProps = ({ alertModalReducer, showSpinnerOverlayReducer }) => ({
  showAlertModal: alertModalReducer.showAlertModal,
  showSpinnerOverlay: showSpinnerOverlayReducer.showSpinnerOverlay
})

class RootContainer extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <AlertModal visible={this.props.showAlertModal} />
        <SpinnerOverlay visible={this.props.showSpinnerOverlay} overlayColor={colors.semiTransparentDark} />
        <StatusBar backgroundColor={colors.main} />
        <NavigationRouter />
      </View>
    )
  }
}

export default connect(mapStateToProps)(RootContainer)