import React from 'react'
import { View, ListView, StyleSheet, Text } from 'react-native'
import { List, ListItem } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'

//temp data
import chatListData from '../../data/chatListData'

const list = chatListData

export default class ChatList extends React.Component {
    constructor () {
        super();
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2 });

        this.state = {
            dataSource: ds.cloneWithRows(list)
        }
    }

    renderRow (rowData, sectionID) {
        return (
        <ListItem
            roundAvatar
            key={sectionID}
            title={rowData.name}
            titleStyle={{fontSize: 12}}
            subtitle={
                <View style={styles.subtitleView}>
                    <Text style={styles.foodtext}>{rowData.food_title}</Text>
                    <Text style={styles.message}>{rowData.last_message}</Text>
                </View>
            }
            rightTitle={rowData.date}
            rightTitleStyle={{fontSize: 12}}
            rightTitleContainerStyle={{justifyContent: 'flex-start', flex: 0,}}
            avatar={{uri:rowData.avatar_url}}
            hideChevron={true}
            underlayColor={'#dddddd'}            
            onPress={() => Actions.chatPage()}
            />
        )
      }

    render () {
        return (
            <View>
                <List containerStyle={{marginTop: 0}}>
                    <ListView
                        renderRow={this.renderRow}
                        dataSource={this.state.dataSource}/>
                </List>
            </View>
        )
    }
}

styles = StyleSheet.create({
    subtitleView: {
        flex: 2
    },
    message: {
      paddingLeft: 10,
      fontSize:13,
    },
    foodtext: {
        paddingTop: 5,
        fontSize:15, 
        fontWeight: 'bold', 
        color: 'black',
        paddingLeft: 10
    }
  })