import React from 'react'
import { 
  View, 
  TouchableOpacity, 
  Text,
  TextInput,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { NavBarWithBackandOptions } from '../../navigation/NavBars'
import ProfileImage from '../../components/ProfileImage'
import MenuBanner from '../../components/MenuBanner'

import styles from './styles/ChatPageStyle'
import { colors } from '../../themes'

var sentByFlag = null

// temp
var chat = {
  senderName: 'Shell In!',
  senderImage: 'http://facebookcraze.com/wp-content/uploads/2009/12/funny_profile_pic_for_facebook_rape.jpg',
  menu: {
    menuName: 'Handsome Salad',
    menuImage: 'https://pbs.twimg.com/profile_images/2395928085/tio3omcu9zth8ifroz05_400x400.jpeg',
    menuPrice: 'RM 49.90'
  },
  chats: {
    'Today': [
      {
        sentBy: 'sender',
        time: '10:00 AM',
        'message': 'Hi how is our Handsome Salad? Is is good or bad? let me know what do you think so we can do some improments'
      },
      {
        sentBy: 'receiver',
        time: '10:01 AM',
        'message': 'Is not as handsome as I thought'
      },
      {
        sentBy: 'receiver',
        time: '10:01 AM',
        'message': 'Could you make it handsomer?'
      },
      {
        sentBy: 'sender',
        time: '10:02 AM',
        'message': 'Sure we can make it handsomer. We will put on some make up. Thanks for the feedback!'
      },
    ]
  }
}

const renderChatSingle = function (singleChat) {
  if (singleChat.sentBy == 'sender')
    return renderChatSender(chat.senderImage, singleChat)
  else if (singleChat.sentBy == 'receiver')
    return renderChatReceiver(singleChat)
}
const renderChatSender = function (senderImage, singleChat) {
  if (sentByFlag == singleChat.sentBy) {
    // return chat bubble yang berjujuran
  }
  else {
    sentByFlag = singleChat.sentBy
    return (
      <View style={styles.chatContainer}>
        <View style={styles.senderProfileContainer}>
          <ProfileImage dimension={60} uri={senderImage} type='user' />
        </View>
        <View style={styles.senderChatContainer}>
          <Text style={styles.chatBubble}>
            { singleChat.message }
          </Text>
          <Text style={styles.chatTime}>{ singleChat.time }</Text>
        </View>
      </View>
    )
  }
}
const renderChatReceiver = function (singleChat) {
  sentByFlag = singleChat.sentBy
  return (
    <View style={[styles.chatContainer, styles.chatContainerReceiver]}>
      <View style={styles.receiverChatContainer}>
        <Text style={[styles.chatBubble, styles.chatBubbleReceiver]}>
          { singleChat.message }
        </Text>
        <Text style={[styles.chatTime, { textAlign: 'right' }]}>{ singleChat.time }</Text>
      </View>
    </View>
  )
}

export default class ChatPage extends React.Component {
  renderChatDate(date) {
    return (
      <Text style={styles.chatDate}>{ date }</Text>
    )
  }
  renderChat() {
    var chats = chat.chats
    for (var key in chats) {
      if(chats.hasOwnProperty(key)) {
        // NEED TO CHANGE INSTEAD OF RETURN, BUAT LAIN
        return (
          <View>
            { this.renderChatDate(key) }
            { chats[key].map(function(chat, i) {
              return renderChatSingle(chat)
            }) }
          </View>
        )
      }
    }
  }
  render() {
    var pageTitle = "Chat With " + chat.senderName
    return (
      <View style={styles.container}>
        <NavBarWithBackandOptions backgroundColor={colors.main} iconColor={colors.white} pageTitle={ pageTitle } />
        <MenuBanner menuId='someFoodId' menuImage={chat.menu.menuImage} menuName={chat.menu.menuName} menuPrice={chat.menu.menuPrice}/>
        <ScrollView>
          { this.renderChat() }
        </ScrollView>
          <KeyboardAvoidingView style={styles.inputBoxContainer}>
            <TextInput
              style={styles.inputBox}
              underlineColorAndroid="transparent"
              placeholder="Your message"
            />
            <TouchableOpacity style={styles.imageInput}>
              <Icon name='camera' size={25} color={colors.semiTransparentDark} />
            </TouchableOpacity>
          </KeyboardAvoidingView>
      </View>
    )
  }
}