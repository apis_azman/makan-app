import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  chatDate: {
    padding: 10,
    fontSize: fonts.size.medium,
    color: colors.semiTransparentDark,
    textAlign: 'center'
  },
  inputBoxContainer: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    backgroundColor: '#eee'
  },
  inputBox: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontSize: fonts.size.regular,
    flex: 5
  },
  imageInput: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  chatContainer: { 
    padding: 10, 
    paddingTop: 0, 
    flex: 1, 
    flexDirection: 'row' 
  },
  chatContainerReceiver: {
    justifyContent: 'flex-end'
  },
  senderProfileContainer: {
    height: 60,
    flex: 1
  },
  senderChatContainer: { 
    flex: 4, 
    paddingLeft: 10, 
    justifyContent: 'center' 
  },
  receiverChatContainer: { 
    paddingLeft: 10 
  },
  chatBubble: { 
    paddingBottom: 5, 
    backgroundColor: colors.aquaBlue, 
    color: colors.white,
    padding: 8, 
    paddingTop: 10, 
    paddingBottom: 10, 
    borderRadius: 5
  },
  chatBubbleReceiver: {
    textAlign: 'right',
    color: colors.black,
    backgroundColor: colors.lightGrey
  },
  chatTime: {
    fontSize: fonts.size.tiny
  }
})