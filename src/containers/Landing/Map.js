import React from 'react'
import { View, Dimensions } from 'react-native'
import MapView from 'react-native-maps'
import MapCallout from './MapCallout'
import Styles from './styles/MapStyles'
import FActionButton from '../../components/Buttons/FActionButton'
import ModalBox from '../../components/Modals/ModalBox'
import CustomMarker from '../../components/CustomMarker'
import AdBanner from '../../components/AdBanner'

// temp
// import bannerAds from '../../data/bannerAds'

class Map extends React.Component {

  constructor (props) {
    super(props)
    //Set the array of locations to be displayed on your map.
  
    const locations = [
      { title: 'Kooyteow Haziq', latitude: 2.922592, longitude: 101.642675, description: 'arezeek'  },
      { title: 'Test0', latitude: 2.914191, longitude: 101.641989, description: 'test0'  },
      { title: 'Test1', latitude: 2.915048, longitude: 101.651087, description: 'test1'  },
      { title: 'Test2', latitude: 2.924992, longitude: 101.655722, description: 'test2'  },
      { title: 'Test3', latitude: 2.939907, longitude: 101.645079, description: 'test3'  }
    ]

    const region = { latitude: 2.909543, longitude: 101.650384, latitudeDelta: 0.1, longitudeDelta: 0.1}
    this.state = {
      region,
      locations,
      showUserLocation: true,
      isModalVisible: null
    }
    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentWillReceiveProps (newProps) {
    //to recenter the map on new locations any time the props change
    // this.setState({
    //   region: calculateRegion(newProps.locations, { latPadding: 0.1, longPadding: 0.1 })
    // })
  }

  onRegionChange (newRegion) {
    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta
    // }
    // Fetch new data to fetch new locations when the user changes the currently visible region
  }

  calloutPress (location) {
    //do something when callout is pressed
  }

  renderMapMarkers (location) {
    return (
      <MapView.Marker 
          key={location.title} 
          coordinate={{latitude: location.latitude, longitude: location.longitude}}
          title={location.title}
          description={location.description}
          >
          <CustomMarker />
        {/*<MapCallout location={location} onPress={this.calloutPress} />*/}
      </MapView.Marker>
    )
  }

  _showModal = () => {
    this.setState({ isModalVisible: 1 })
  }

  _nextModal = () => {
    this.setState({ isModalVisible: 2 })
  }

  _hideModal = () => {
  this.setState({ isModalVisible: 0 })
  }

  render () {
    return (
      <View style={Styles.rootContainer}>
        <MapView
          style={Styles.map}
          initialRegion={this.state.region}
          onRegionChangeComplete={this.onRegionChange}
          showsUserLocation={this.state.showUserLocation}
        >
          {this.state.locations.map((location) => this.renderMapMarkers(location))}
        </MapView>
        {/*<View style={Styles.adBannerContainer}>
          <AdBanner height={80} ads={bannerAds}/>
        </View>*/}
        <FActionButton onclick={this._showModal} />
        <ModalBox 
          isModalVisible={this.state.isModalVisible} 
          onPressNext={this._nextModal}
          onPressClose={this._hideModal}/>
      </View>
    )
  }
}

export default Map

