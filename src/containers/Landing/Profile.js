import React from 'react'
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import IconButton from '../../components/Buttons/IconButton'
import FullWidthButton from '../../components/Buttons/FullWidthButton'
import PageBackground from '../../components/PageBackground'
import ProfileImage from '../../components/ProfileImage'
import StoreBanner from '../../components/StoreBanner'
import Food from '../../components/Food'

import styles from './styles/ProfileStyle'
import foodStyles from '../../components/styles/FoodStyle'
import categoriesStyles from './styles/CategoriesStyle'
import { colors, fonts } from '../../themes/'

// dummy data
import followingFoodData from '../../data/followingFoodData'

const mapStateToProps = ({ accountReducer }) => ({
  user: accountReducer.user,
  store: accountReducer.store
})

_onClickFollowing = () => {
  Actions.following({page: 'Following'})
}

_onClickFollower = () => {
  Actions.following({page: 'Followers'})
}

_onClickEditProfile = () => {
  Actions.editProfile()
}

onCreateShop = () => {
  Actions.setupShop()
}

class Profile extends React.Component {
  renderFoodRow() {
    var 
      counter = 0, 
      // foodData = this.props.store.menus ? this.props.store.menus : [], 
      foodData = followingFoodData,
      rowFoods = []

    if(foodData.length > 0) {
      return foodData.map((food, i) => {
        if((foodData.length % 2 != 0) && (i == (foodData.length - 1))) {
          rowFoods[0] = food
          rowFoods[1] = 'empty'
          counter = 2
        }
        else {
          rowFoods[counter] = food
          counter++
        }

        if(counter == 2) {
          counter = 0
          return (
            <View key={i} style={categoriesStyles.categoriesRow}>
              { this.renderMenu(rowFoods) }
            </View>
          )
        }
      })
    }
    else {
      return (
        <View style={[styles.menuMessageContainer, { paddingTop: 0 }]}>
          <Icon name='cursor-pointer' size={40} color={colors.main} />
          <Text style={styles.menuMessageText}>
            Click on that to create a menu!
          </Text>
        </View>
      )
    }
  }
  renderMenu(rowFoods) {
    return rowFoods.map((food, i) => {
      return (
        <Food key={i} food={food} />
      )
    })
  }
  renderMenuSection() {
    if(!this.props.store)
      return (
        <View>
          <View style={styles.menuMessageContainer}>
            <Icon name='silverware' size={40} color={colors.main} />
            <Text style={[styles.menuMessageText, {fontWeight: '500'}]}>
              You haven't started any selling yet!
            </Text>
            <Text style={styles.menuMessageText}>
              Creating a menu takes just 30 seconds. Start earning by selling the things everyone loves. Food!
            </Text>
          </View>
          <FullWidthButton label='OK, IM READY TO COOK' action={onCreateShop} />
        </View>
      )
    else 
      return (
        <View style={{ backgroundColor: colors.lightGrey }}>
          <StoreBanner showEditStore backgroundColor={colors.lightGrey} store={this.props.store} />
          <TouchableOpacity style={styles.addMenuButton} onPress={() => Actions.addMenu()}>
            <Icon size={16} name='pizza'> New Menu</Icon>
          </TouchableOpacity>
          { this.renderFoodRow() }
        </View>
      )
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <PageBackground>
          <View style={styles.profileContainer}>
            <View style={styles.editButtonContainer}>
              <IconButton style={styles.editButtonContainer} iconName='account-circle' iconSize={25} iconColor={colors.white} action={_onClickEditProfile} />
            </View>
            <ProfileImage dimension={120} uri={this.props.user.imageUrl} type='user' />
            <Text style={styles.profileName}>{ this.props.user.name ? this.props.user.name : this.props.user.email }</Text>
            {/* <IconAwesome style={styles.profileLocation} name='map-marker' size={13} color={colors.black}>  { user.lastLocation }</IconAwesome> */}
            {/* <Text style={styles.profileDate}>Member since { user.registeredOn }</Text> */}
          </View>
          <View style={styles.countsSection}>
            <TouchableOpacity style={styles.countsContainer}>
              <Text style={styles.countsNumber}>{ this.props.user.orders ? this.props.user.orders : 0 }</Text>
              <Text style={styles.countsLabel}>Orders</Text>
            </TouchableOpacity>
            {/*
            <TouchableOpacity style={styles.countsContainer} onPress={_onClickFollowing}>
              <Text style={styles.countsNumber}>{ this.props.user.following ? this.props.user.following : 0 }</Text>
              <Text style={styles.countsLabel}>Following</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.countsContainer} onPress={_onClickFollower}>
              <Text style={styles.countsNumber}>{ this.props.user.followers ? this.props.user.followers : 0 }</Text>
              <Text style={styles.countsLabel}>Followers</Text>
            </TouchableOpacity>
            */}
          </View>
          { this.renderMenuSection() }
        </PageBackground>
      </ScrollView>
    )
  }
}

export default connect(mapStateToProps)(Profile)