import React from 'react'
import { 
  ScrollView, 
  Text, 
  View, 
  TouchableOpacity 
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import style from './styles/ActivityStyle'

// temp
const activityList = {
  Today: [
    {
      message: 'Rogayah started following you!',
      type: 'follow',
      time: '12.00 pm',
      action: () => alert('follow!')
    },
    {
      message: 'Junaidah Sameon ordered Nasi Ayam',
      type: 'order',
      time: '1.10 am',
      action: () => alert('order!')
    }
  ],
  Yesterday: [
    {
      message: 'Sameon started following you!',
      type: 'follow',
      time: '12.00 pm',
      action: () => alert('follow!')
    },
    {
      message: 'Abu Sameon ordered Koteow',
      type: 'order',
      time: '1.10 am',
      action: () => alert('order!')
    }
  ],
  'Tue, 20 Aug 2017': [
    {
      message: 'Jemah started following you!',
      type: 'follow',
      time: '12.30 pm',
      action: () => alert('follow!')
    },
    {
      message: 'Ayob sent you a message',
      type: 'message',
      time: '1.15 pm',
      action: () => alert('order!')
    }
  ]
}

class Activity extends React.Component {
  renderSectionByTime(activitySeparatorKeys) {
    if(activitySeparatorKeys.length == 0)
      return (
        <View style={style.messageContainer}>
          <Icon name='emoticon-sad' size={50}/>
          <Text style={style.messageText}>It's pretty lonely in here. You have no activity.</Text>
        </View>
      )
    else 
      return activitySeparatorKeys.map((activitySeparatorKey, i) => {
        return (
          <View key={i}>
            <Text style={style.sectionTitle}>{ activitySeparatorKey }</Text>
            { this.renderActivity(activityList[activitySeparatorKey]) }
          </View>
        )
      })
  }
  renderActivity(activityItems) {
    return activityItems.map((activity, i) => {
      var icon;
      if(activity.type == 'follow') icon = 'account-multiple'
      else if(activity.type == 'order') icon = 'silverware'
      else if(activity.type == 'message') icon = 'forum'
      else icon = 'flash'

      return (
        <TouchableOpacity key={i} style={style.activityListItem} onPress={activity.action}>
          <View style={style.activityIconContainer}>
            <Icon name={icon} size={15}/>
          </View>
          <Text style={style.activityMessageContainer}>
            { activity.message }
          </Text>
          <View style={style.timeLabelContainer}>
            <Text style={style.timeLabelText}>{ activity.time }</Text>
          </View>
        </TouchableOpacity>
      )
    });
  }
  render() {
    // get activity keys/date separator
    var activitySeparatorKeys = []
    for(var key in activityList)
      if(activityList.hasOwnProperty(key))
        activitySeparatorKeys.push(key)

    return (
      <View style={style.container}>
        { this.renderSectionByTime(activitySeparatorKeys) }
      </View>
    );
  }
}

export default Activity