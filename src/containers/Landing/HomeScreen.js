import React from 'react'
import { connect } from 'react-redux'

import PageBackground from '../../components/PageBackground'
import Categories from './Categories'
import Map from './Map'

const mapStateToProps = ({ homeScreenModeReducer }) => ({
  homeScreenMode: homeScreenModeReducer.homeScreenMode
})

class HomeScreen extends React.Component {
  renderPageContent() {
    if(this.props.homeScreenMode == 'Categories')
      return <Categories />
    else if (this.props.homeScreenMode == 'Map')
      return <Map />
  }
  render() {
    return (
      <PageBackground>
        { this.renderPageContent() }
      </PageBackground>
    )
  }
}

export default connect(mapStateToProps)(HomeScreen)