import React from 'react'
import { ScrollView, View, Text } from 'react-native'
import { connect } from 'react-redux'

import styles from './styles/CategoriesStyle'

import AdBanner from '../../components/AdBanner'
import Category from '../../components/Category'
import FActionButton from '../../components/Buttons/FActionButton'
import ModalBox from '../../components/Modals/ModalBox'

import menuServices from '../../services/menuServices'

// temp
//import bannerAds from '../../data/bannerAds'

// dummy data
//import categoriesData from '../../data/categories'

const mapStateToProps = ({ accountReducer }) => ({
  auth: accountReducer.token
})

class Categories extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryList: [],
      isModalVisible: false
    }
  }

  componentWillMount() {
    var that = this

    menuServices.getCategories(this.props.auth, function(categories) {
      var categoryList = [{
        label: 'All',
        background: 'http://lh3.ggpht.com/K1BiBysQYph8tAbHsWVF4afo-mZ7vipZOWeYs91YONXIqmuMot_Cea4-a-NjE6JBA4DJ-rUtIRx0IX0QW5xupTAf=s300-c',
        id: ''
      }]
      for(var i=0; i<categories.categories.length; i++)
        categoryList.push({
          label: categories.categories[i].label,
          background: categories.categories[i].imageUrl,
          id: categories.categories[i]._id
        })
      that.setState({ categoryList: categoryList })
    })
  }

  _showModal = () => {
    this.setState({ isModalVisible: true })
  }

  _hideModal = () => {
    this.setState({ isModalVisible: false })
  }

  renderCategoriesRow() {
    var counter = 0, categories = this.state.categoryList, rowCategories = []

    return categories.map((category, i) => {

      if((categories.length % 2 != 0) && (i == (categories.length - 1))) {
        rowCategories[0] = { label: category.label, background: category.background, id: category.id }
        rowCategories[1] = { label: 'empty' }
        counter = 2
      }
      else {
        rowCategories[counter] = { label: category.label, background: category.background, id: category.id }
        counter++
      }

      if(counter == 2) {
        counter = 0
        return (
          <View key={i} style={styles.categoriesRow}>
            { this.renderCategory(rowCategories) }
          </View>
        )
      }
    })
  }
  renderCategory(rowCategories) {
    return rowCategories.map((category, i) => {
      return (
        <Category key={i} label={category.label} background={category.background} id={category.id} />
      )
    })
  }

  render() {
    return (
      <View style={styles.rootContainer}>
        <Text style={styles.pageLabel}>Food Categories</Text>
        <ScrollView style={styles.container}>
        {/*
          <View style={styles.adBanner}>
            <AdBanner height={200} ads={bannerAds}/>
          </View>
        */}
          { this.renderCategoriesRow() }
        </ScrollView>
        <FActionButton onclick={this._showModal} />
        <ModalBox 
            isModalVisible={this.state.isModalVisible} 
            title="But before that!" 
            text1="We notice that you haven't provide us with your phone number. This is to ensure that both parties are protected in all transactions."
            text2="Kindly input your phone number below before we proceed. "
            text3="By taping NEXT, I confirm that I agree to the Privacy Policy!!!"
            textInput={true}
            buttonTitle="NEXT"
            onPress={this._hideModal}
            />

      </View>
    )
  }
}

export default connect(mapStateToProps, {})(Categories)
