import React from 'react'
import { 
  ScrollView,
  View,
  Text
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'

import profileStyles from './styles/ProfileStyle'
import styles from './styles/KitchenStyle'
import { colors } from '../../themes'

import StoreBanner from '../../components/StoreBanner'

const mapStateToProps = ({ accountReducer }) => ({
  store: accountReducer.store
})

class Kitchen extends React.Component {
  renderOrderSection() {
    // if got order, update here
    return (
        <View>
          <View style={profileStyles.menuMessageContainer}>
            <Text style={[profileStyles.menuMessageText, {fontWeight: '500'}]}>
              Opps looks like you don't have any orders yet
            </Text>
            <Text style={profileStyles.menuMessageText}>
              If you have not create any menu yet, go to Profile then click on New Menu and start selling
            </Text>
          </View>
        </View>
      )
  }
  render() {
    return (
      <ScrollView>
        <StoreBanner fontColor={colors.white} backgroundColor={colors.main} store={this.props.store} />
        <View style={styles.tabSection}>
          <Text style={styles.tabSectionTitle}>All Orders</Text>
        </View>
        { this.renderOrderSection() }
      </ScrollView>
    )
  }
}

export default connect(mapStateToProps)(Kitchen)