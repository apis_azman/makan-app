import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes/'

export default StyleSheet.create({
  container: {
    padding: 5
  },
  messageContainer: { 
    marginTop: 30, 
    alignItems: 'center' 
  },
  messageText: {
    fontSize: 15,
    marginTop: 10
  },
  sectionTitle: { 
    marginTop: 5, 
    marginBottom: 3, 
    fontWeight: '500' 
  },
  activityIconContainer: { 
    flex: 1, 
    justifyContent: 'center' 
  },
  activityMessageContainer: {
    flex: 10
  },
  timeLabelContainer: { 
    flex: 2, 
    justifyContent: 'center', 
    alignItems: 'flex-end' 
  },
  timeLabelText: {
    fontSize: 11
  },
  activityListItem: { 
    flexDirection: 'row',
    minHeight: 30, 
    marginBottom: 3,
    backgroundColor: colors.lightGrey,
    borderWidth: 1, 
    borderColor: colors.lightGrey, 
    padding: 10, 
    justifyContent: 'center' 
  }
})