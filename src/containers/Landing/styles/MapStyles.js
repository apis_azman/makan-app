import { StyleSheet } from 'react-native'

import { colors } from '../../../themes'

export default StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  rootContainer: {
    flex: 1
  },
  adBannerContainer: {
    margin: 10, 
    height: 80,
    backgroundColor: colors.semiTransparentDark
  }
})
