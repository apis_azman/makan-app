import { StyleSheet } from 'react-native'

import { colors } from '../../../themes'

export default StyleSheet.create({
  pageLabel: { 
    color: colors.white, 
    padding: 10, 
    textAlign: 'center', 
    fontWeight: '500' 
  },
  container: {
    backgroundColor: colors.lightGrey,
    paddingBottom: 3
  },
  adBanner: {
    height: 200,
    marginBottom: 2,
    flex: 1
  },
  categoriesRow: { 
    flex: 1, 
    flexDirection: 'row',
    marginRight: 2,
    marginLeft: 2
  },
  rootContainer: {
    flex: 1,
  }
})