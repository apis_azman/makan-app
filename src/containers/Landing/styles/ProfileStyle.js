
import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  profileContainer: {
    height: 250, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  editButtonContainer: {
    position: 'absolute', 
    top: 20, 
    right: 20
  },
  profileName: {
    color: colors.white, 
    fontSize: fonts.size.h6, 
    fontFamily: fonts.type.bold, 
    marginTop: 15
  },
  profileLocation: {
    marginTop: 8,
    color: colors.semiTransparent
  },
  profileDate: {
    marginTop: 8,
    fontFamily: fonts.type.italic,
    fontSize: 12,
    color: colors.semiTransparent
  },
  countsSection: {
    backgroundColor: colors.mainDark,
    flexDirection: 'row',
    paddingTop: 12,
    paddingBottom: 15
  },
  countsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  countsNumber: {
    fontFamily: fonts.type.bold,
    fontSize: 25,
    color: colors.white
  },
  countsLabel: {
    fontFamily: fonts.type.regular,
    color: colors.semiTransparent
  },
  addMenuButton: { 
    backgroundColor: colors.white, 
    height: 50, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  menuMessageContainer: {
    backgroundColor: colors.white, 
    justifyContent: 'center', 
    alignItems: 'center', 
    paddingTop: 23, 
    paddingBottom: 23
  },
  menuMessageText: {
    textAlign: 'center',
    fontFamily: fonts.type.regular,
    fontSize: fonts.size.regular,
    paddingTop: 20
  }
})