import { StyleSheet } from 'react-native'

import { colors } from '../../../themes/'

export default StyleSheet.create({
  tabSection: { 
    height: 40, 
    backgroundColor: colors.main, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  tabSectionTitle: {
    color: colors.white
  }
})