import React from 'react'
import { View, ListView, Button } from 'react-native'
import { List, ListItem } from 'react-native-elements'
import { NavBarWithBack } from '../../navigation/NavBars'

//temp data
import followingData from '../../data/followingData'

const list = followingData

export default class Following extends React.Component {
    constructor () {
        super();
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2 });

        this.state = {
            dataSource: ds.cloneWithRows(list)
        }
    }

    renderRow (rowData, sectionID) {
        return (
        <ListItem
            roundAvatar
            key={sectionID}
            title={rowData.name}
            subtitle={rowData.subtitle}
            avatar={{uri:rowData.avatar_url}}
            underlayColor={'#dddddd'}            
            onPress={() => alert('user clicked')}
            rightIcon={
                <Button color="#f57365" title={"Following"} onPress={()=> alert('button pressed')}/>
            }
            />
        )
      }

    render () {
        return (
            <View>
                <NavBarWithBack pageTitle={this.props.page} />
                <List containerStyle={{marginTop: 0}}>
                    <ListView
                        renderRow={this.renderRow}
                        dataSource={this.state.dataSource} />
                </List>
            </View>
        )
    }
}