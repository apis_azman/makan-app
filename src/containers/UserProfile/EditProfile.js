import React from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import DropdownPicker from '../../components/Inputs/DropdownPicker'

import userServices from '../../services/userServices'

import addMenuStyle from '../Food/styles/AddMenuStyle'
import foodDetailsStyle from '../Food/styles/FoodDetailsStyle'

import ListSection from '../../components/ListSection'
import FormField from '../../components/Inputs/FormField'
import FullWidthButton from '../../components/Buttons/FullWidthButton'
import PhotoInput from '../../components/Inputs/PhotoInput'
import { NavBarWithBack } from '../../navigation/NavBars'

const mapStateToProps = ({ accountReducer }) => ({
  user: accountReducer.user,
  auth: accountReducer.token
})

const genderOptions = [ 'Male', 'Female', 'Sajat' ]

// ISSUES HERE
// api does not support photo yet
// address should follow shop address, multiline input, postcode etc.

class EditProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      phone: '',
      photo: '',
      address: '',
      gender: ''
    }
  }
  componentWillMount() {
    let that = this 

    userServices.getUser(this.props.user.id, this.props.auth, function(res) {
      that.setState({
        name: res.name,
        photo: '',
        phone: res.phone,
        address: res.address,
        gender: res.gender
      })
    })
  }

  onChangeValue(key, value) {
    this.setState({
      [key]: value
    })
  }
  renderTextInput(placeholder, key, defaultValue) {
    return (
      <TextInput 
        style={addMenuStyle.textInput} 
        underlineColorAndroid='rgba(0,0,0,0)' 
        placeholder={placeholder} 
        defaultValue={defaultValue} 
        onChangeText={(itemValue) => this.onChangeValue(key, itemValue)}
      ></TextInput>
    )
  }
  render() {
    return (
      <View>
        <NavBarWithBack pageTitle='Edit Profile' />
        <ScrollView>
          <View style={addMenuStyle.photoSection}>
            <PhotoInput inputIcon='face' photoLabel='Profile Picture' returnImg={(img) => this.onChangeValue('photo', img)} />
          </View>
          <ListSection>
            <Text style={foodDetailsStyle.sectionTitle}>My Details</Text>
            <FormField label='Name' inputComponent={this.renderTextInput('Your name', 'name', this.state.name)} />
            <FormField label='Mobile' inputComponent={this.renderTextInput('Your phone number', 'phone', this.state.phone)} />
            <FormField label='Address' inputComponent={this.renderTextInput('Your display address', 'address', this.state.address)} />
            <FormField 
              label='Gender'
              inputComponent={
                <DropdownPicker options={genderOptions} value={this.state.gender} onChangeValue={(itemValue) => this.onChangeValue('gender', itemValue)} />
              }
            />
          </ListSection>
          <FullWidthButton label='SAVE' action={() => { alert('Will implement save') }} />
          <View style={addMenuStyle.footerOverhead} />
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps)(EditProfile)
