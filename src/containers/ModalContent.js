import React, { Component } from 'react';
import { Rating, AirbnbRating } from 'react-native-ratings'
import IconButton from '../components/Buttons/IconButton'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class ModalBox extends React.Component {
  ratingDone(rating) {
    console.log("Rating is: " + rating)
  }

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress} style={styles.buttonContainer}>
        <Text style={styles.buttonText}>
            {text}
        </Text>
    </TouchableOpacity>
  );

  renderModal() {
    if(this.props.content === 1){
      return(
        <View style={styles.modalContent}>
            <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                <TouchableOpacity onPress={this.props.onClose}>                
                        <Icon name='close' size={21} color='gray' />
                </TouchableOpacity>
            </View>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>But before that!</Text>
            <Text>{"\n"}We notice that you haven't provide us with your phone number. This is to ensure that both parties are protected in all transactions.</Text>
            <Text>{"\n"}Kindly input your phone number below before we proceed. </Text>

            <TextInput 
                placeholder="Phone Number" 
                placeholderTextColor="gray"
                returnKeyType="next" 
                keyboardType="phone-pad"
                autoCorrect={false}
                underlineColorAndroid='gray'
                selectionColor='gray'
            />

            <Text>{"\n"}By taping NEXT, I confirm that I agree to the Privacy Policy!</Text>

            { this._renderButton('Next', this.props.onClick) }  
        </View>
      )
    } else if (this.props.content === 2) {
      return(
        <View style={styles.modalContent}>
            <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                <TouchableOpacity onPress={this.props.onClose}>                
                        <Icon name='close' size={21} color='gray' />
                </TouchableOpacity>
            </View>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Do you like the food? </Text>

            <AirbnbRating
                count={5}
                showRating
                onFinishRating={this.ratingDone}
                />

            { this._renderButton('Close', this.props.onClick) }                   
        
        </View>
      )
    } else {
      return(null)
    }
    
  }

  render() {
    return (
        <View>
            { this.renderModal() }
        </View>
    )
  }
}

const styles = StyleSheet.create({
    modalContent: {
        // flex: 1,
        // alignSelf: "center",
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    buttonContainer: {
        backgroundColor: 'rgb(245, 115, 101)',
        paddingVertical: 15,
        borderRadius: 5,
        marginTop: 10
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700'
    },
});