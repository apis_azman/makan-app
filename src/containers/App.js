import React from 'react'
import { Provider } from 'react-redux'

import { createStore, compose, applyMiddleware } from 'redux'
import appReducers from '../redux/reducers'

import RootContainer from './RootContainer'

import ReduxThunk from 'redux-thunk'
import SplashScreen from 'react-native-splash-screen'

export const reduxStore = createStore(
  appReducers, 
  {}, 
  compose(
    applyMiddleware(ReduxThunk)
))

export default class App extends React.Component {
  componentWillMount () {
      //do something
  }

  componentDidMount () {
      SplashScreen.hide()
  }

  render() {
    return (
      <Provider store={reduxStore}>
        <RootContainer />
      </Provider>
    )
  }
}