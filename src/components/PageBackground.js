import React from 'react'
import { View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import styles from './styles/PageBackgroundStyle'
import { colors } from '../themes/'

export default class PageBackground extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <LinearGradient colors={[colors.main, colors.mainGradientEnd]} style={styles.container}>
        { this.props.children }
      </LinearGradient>
    )
  }
}