import React from 'react'
import { View, Image } from 'react-native'
import { colors } from '../themes'

export default class CustomMarker extends React.Component {
    render () {
        return (
            <View style={{backgroundColor: colors.white, borderRadius: 50, padding: 2}}>
                <View style={{backgroundColor: colors.accent2, borderRadius: 50, padding: 3}}>
                    <Image 
                        source={require('../assets/images/logo-white-transparent.png')} 
                        style={{height: 15, width: 15}}/>
                </View>
            </View>
        )
    }
}
