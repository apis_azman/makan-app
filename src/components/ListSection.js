import React from 'react'
import { View } from 'react-native'

import { colors } from '../themes'
import styles from './styles/ListSectionStyle'

export default class DetailsSection extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        { this.props.children }
      </View>
    )
  }
}