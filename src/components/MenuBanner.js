import React from 'react'
import {
  TouchableOpacity,
  View,
  Text
} from 'react-native'
import Image from 'react-native-image-progress'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Actions } from 'react-native-router-flux'

import styles from './styles/MenuBannerStyle'
import { colors } from '../themes'

export default class MenuBanner extends React.Component {
  render() {
    return (
      <TouchableOpacity style={styles.menuDetailsContainer} onPress={() => Actions.foodDetails({foodId: this.props.menuId})}>
        <View style={styles.menuImage}>
          <Image style={styles.menuImage} source={{uri: this.props.menuImage}} resizeMode='cover' />
        </View>
        <View style={styles.menuDetails}>
          <View style={styles.menuTitleContainer}>
            <Text style={styles.menuTitle}>{ this.props.menuName }</Text>
          </View>
          <View style={styles.menuPriceContainer}>
            <Text style={styles.menuPrice}>{ this.props.menuPrice }</Text>
          </View>
        </View>
        <View style={styles.menuActions}>
          <TouchableOpacity style={{ marginRight: 10 }}>
            <Icon name='share' size={25} color={colors.accent2} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name='heart-outline' size={25} color={colors.accent2} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
} 