import React from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  Image
} from 'react-native'
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons'
import ImagePicker from 'react-native-image-picker'

import addMenuStyle from '../../containers/Food/styles/AddMenuStyle'

var options = {
  title: 'Select Profile Picture',
  // customButtons: [
  //   {name: 'fb', title: 'Choose Photo from Facebook'},
  // ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class PhotoInput extends React.Component {  
  constructor(props){
    super(props);
    this.state = {
      avatarSource: null
    }
  }
  componentWillMount() {
    if(this.props.value) {
      let avatarSource = { uri: this.props.value }
      this.setState({
        avatarSource: avatarSource
      })
    }
  }

  setImageUri(response) {
    this.setState({
      avatarSource: { uri: response.uri }
    });

    this.props.returnImg(response)
  }

  _onPressProfilePicture() {
    ImagePicker.showImagePicker(options, (response) => {
      if(!response.didCancel && !response.error && !response.customButton)
        this.setImageUri(response)
    })
  }

  render() {
    let image;
    if(this.state.avatarSource)
      image = (<Image 
        source={this.state.avatarSource}
        style={{height: 130, width: 130}} 
      />)
    else
      image = (<IconMaterial name={this.props.inputIcon} size={30}/>)

    return (
      <View>
        <TouchableOpacity style={addMenuStyle.photoBox} onPress={this._onPressProfilePicture.bind(this)}>
          { image }
        </TouchableOpacity>
        <View style={addMenuStyle.photoLabel}>
          <Text>{ this.props.photoLabel }</Text>
        </View>
      </View>
    )
  }
}