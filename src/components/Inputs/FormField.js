import React from 'react'
import { View, Text } from 'react-native'

import style from './styles/FormFieldStyle'

export default class FormField extends React.Component {
  render() {
    return (
      <View style={style.fieldContainer}>
        <View style={this.props.isCheckBox ? style.labelContainerCheck : style.labelContainer}>
          <Text>{ this.props.label }</Text>
        </View>
        <View style={this.props.isCheckBox ? style.inputContainerCheck : style.inputContainer}>
          { this.props.inputComponent }
        </View>
      </View>
    )
  }
}