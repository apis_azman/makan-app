import React from 'react'
import { Picker } from 'react-native'

export default class DropdownPicker extends React.Component {
  renderOptions(options) {
    return options.map((option, i) => (
      <Picker.Item key={i} label={option} value={option} />
    ))
  }
  render() {
    return (
      <Picker
        mode='dropdown'
        selectedValue={this.props.value}
        onValueChange={(itemValue, itemIndex) => this.props.onChangeValue(itemValue)}
      >
        { this.renderOptions(this.props.options) }
      </Picker>
    )
  }
}