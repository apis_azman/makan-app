import { StyleSheet } from 'react-native'

import { colors } from '../../../themes/'

export default StyleSheet.create({
  menuContainer: {
    flex: 1
  },
  menuLabelContainer: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },
  menuOption: { 
    flex: 1, 
    alignItems: 'center' 
  },
  menuOptionText: { 
    fontWeight: '500' 
  },
  filterBarContainer: { 
    flexDirection: 'row', 
    height: 50, 
    marginTop: 5, 
    marginBottom: 5, 
    backgroundColor: 'white'
  }
})