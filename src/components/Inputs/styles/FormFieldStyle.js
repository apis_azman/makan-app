import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes'

export default StyleSheet.create({
  fieldContainer: { 
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1, 
    borderColor: colors.lightGrey 
  },
  labelContainer: {
    flex: 2
  },
  labelContainerCheck: {
    flex: 5
  },
  inputContainer: {
    flex: 5
  },
  inputContainerCheck: {
    flex: 2,
    alignItems: 'flex-end'
  }
})