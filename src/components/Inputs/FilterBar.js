import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import styles from './styles/FilterBarStyle'

class FilterBar extends React.Component {
  state = {
    location: 'My Location',
    category: 'Following',
    filter: 'Popular'
  }
  handleLocationChange = (option) => {
    this.setState({ location: option })
    // do api call
  }
  handleCategoryChange = (option) => {
    this.setState({ category: option })
    // do api call
  }
  handleFilterChange = (option) => {
    this.setState({ filter: option })
    // do api call
  }
  renderFilterSection(label, icon, selectedValueKey, options, action) {
    return (
      <TouchableOpacity style={styles.menuContainer} onPress={() => { Actions.menuOption({ label: label, options: options, action: action }) }}>
        <View style={styles.menuLabelContainer}>
          <Text>
            <Icon name={ icon } color='black' size={13}/>
            &nbsp; { label }
          </Text>
        </View>
        <View style={styles.menuOption}>
          <Text style={styles.menuOptionText}>
            { this.state[selectedValueKey] }
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <View style={styles.filterBarContainer}>
        { this.renderFilterSection('10KM From', 'map-marker', 'location',['All', 'My Location', 'Your Location'], this.handleLocationChange) }
        { this.renderFilterSection('CATEGORY', 'cards', 'category', ['All', 'Following', 'Favourited', 'Malay', 'Chinese'], this.handleCategoryChange) }
        { this.renderFilterSection('FILTER', 'filter-variant', 'filter', ['All', 'Popular', 'Not Popular'], this.handleFilterChange) }
      </View>
    )
  }
}

export default FilterBar