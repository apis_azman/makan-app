import React from 'react'
import { Text, View, ScrollView, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { NavBarWithBack } from '../navigation/NavBars'
import TextButton from './Buttons/TextButton'

import styles from './styles/MenuOptionStyle'

class MenuOption extends React.Component {
  handleAction(option) {
    this.props.action(option);
    Actions.pop();
  }
  renderOptions() {
    return this.props.options.map((option, i) => {
      return (
        <TouchableOpacity key={i} style={styles.optionButton} onPress={() => this.handleAction(option)}>
          <Text style={styles.optionLabel}>{ option }</Text>
        </TouchableOpacity>
      )
    })
  }
  render() {
    return (
      <View>
        <NavBarWithBack pageTitle={this.props.label}/>
        <ScrollView style={styles.container}>
          { this.renderOptions() }
        </ScrollView>
      </View>
    )
  }
}

export default MenuOption
