import React from 'react'
import { View } from 'react-native'
import Image from 'react-native-image-progress'
import Swiper from 'react-native-swiper'

import { colors } from '../themes/'

export default class AdBanner extends React.Component {
  renderAds() {
    return this.props.ads.map((bannerAd, i) => {
      return <Image key={i} style={{flex: 1}} source={{uri: bannerAd}} resizeMode='cover' />
    })
  }
  render() {
    return (
      <Swiper height={this.props.height} autoplay autoplayTimeout={4} dotColor={colors.semiTransparent} activeDotColor={colors.white}>
        { this.renderAds() }
      </Swiper>
    )
  }
}