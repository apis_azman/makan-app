import React from 'react'
import {
  TouchableOpacity,
  View,
  Text
} from 'react-native'
import Image from 'react-native-image-progress'
import { Actions } from 'react-native-router-flux'

import styles from './styles/CategoryStyle'

export default class Category extends React.Component {
  render() {
    if(this.props.label != 'empty')
      return (
        <TouchableOpacity style={styles.container} onPress={() => Actions.foodCatalog({page: this.props.label, id: this.props.id})}>
          <Image source={{uri: this.props.background}} resizeMode='cover' style={styles.backgroundImg}>
            <View style={styles.labelContainer} />
          </Image>
          <View style={styles.overlay}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>{ this.props.label.toUpperCase() }</Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    else
      return (
        <View style={styles.emptyContainer}>
        </View>
      )
  }
}