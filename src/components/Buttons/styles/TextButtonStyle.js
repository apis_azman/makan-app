import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes/'

export default StyleSheet.create({
  button: { 
    flex: 1,
    minHeight: 40,
    minWidth: 90,
    backgroundColor: colors.accent2,
    justifyContent: 'center'
  },
  label: {
    textAlign: 'center',
    color: colors.white,
    fontSize: fonts.size.regular,
    fontWeight: 'bold'
  }
})