import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../../themes/'

export default StyleSheet.create({
  buttonContainer: {
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: colors.accent2
  },
  buttonLabel: {
    textAlign: 'center',
    fontFamily: fonts.type.bold,
    fontSize: fonts.size.h5,
    color: colors.white
  }
})