import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    buttonContainer: {
        padding: 0,
        // backgroundColor: 'rgb(66,93,174)',
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    icon: {
        flex: 1,
        alignItems: 'flex-start',
        padding: 10,
        opacity: 0.9
    },
    textWrapper: {
        flex: 4,
        marginLeft: 10,
        padding: 10,
    },
    text: {
        fontSize: 18,
        color: '#ffffff',
        fontWeight: '500',
        opacity: 0.9
    }
})