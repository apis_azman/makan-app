import React, { Component } from 'react';
import { StyleSheet, Image, View } from 'react-native'

import ActionButton from 'react-native-action-button'

const actionImage = require('../../assets/images/logo-white-transparent.png')

export default class FActionButton extends React.Component {
    render () {
        return (
                <ActionButton 
                    buttonColor="rgb(245, 115, 101)" 
                    icon={<Image source={actionImage} style={{ borderRadius: 50, height: 35, width: 35 }} />} 
                    position="center"
                    onPress={this.props.onclick}/>
        )
    }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});