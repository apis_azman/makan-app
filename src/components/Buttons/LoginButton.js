import React from 'react'
import { Text, View, TouchableOpacity, TouchableHighlight, StyleSheet } from 'react-native'
import Styles from './styles/LoginButtonStyle'
import FBSDK, { LoginManager, AccessToken } from 'react-native-fbsdk'
import Icon from 'react-native-vector-icons/FontAwesome'
import { colors } from '../../themes'

class LoginButton extends React.Component {
    

    render() {
        return (
                <TouchableOpacity onPress={this.props.onclick} style={[Styles.buttonContainer, {backgroundColor: this.props.color}]}>
                    <View style={Styles.icon}>
                        <Icon name={this.props.icon} size={25} color={colors.white} />
                    </View>
                    <View style={Styles.textWrapper}>
                        <Text style={Styles.text}>{this.props.text}</Text>
                    </View>
                </TouchableOpacity>
        )
    }
}

export default LoginButton