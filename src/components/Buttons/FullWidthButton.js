import React from 'react'
import { Text, TouchableOpacity } from 'react-native'

import styles from './styles/FullWidthButtonStyle'

export default class FullWidthButton extends React.Component {
  render() {
    return (
      <TouchableOpacity style={styles.buttonContainer} onPress={this.props.action}>
        <Text style={styles.buttonLabel}>{ this.props.label }</Text>
      </TouchableOpacity>
    )
  }
}