import React from 'react'
import { TouchableOpacity, Text } from 'react-native'

import styles from './styles/TextButtonStyle'

export default class TextButton extends React.Component {
  render() {
    return (
      <TouchableOpacity style={styles.button} onPress={this.props.action}>
        <Text style={styles.label}>{this.props.label}</Text>
      </TouchableOpacity>
    )
  }
}