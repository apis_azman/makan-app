import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { TouchableOpacity } from 'react-native'

import styles from './styles/IconButtonStyle'

export default class IconButton extends React.Component {
  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.action}>
        <Icon name={this.props.iconName} size={this.props.iconSize} color={this.props.iconColor} />
      </TouchableOpacity>
    )
  }
}