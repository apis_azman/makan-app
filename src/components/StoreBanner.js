import React from 'react'
import {
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconAwesome from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'

import { colors } from '../themes'

import ProfileImage from './ProfileImage'

export default class StoreBanner extends React.Component {
  openStoreProfile(storeId) {
    Actions.shopProfile({ storeId: storeId })
  }
  openEditStore(storeId) {
    Actions.setupShop({ storeId: storeId, action: 'edit' })
  }

  renderEditStoreButton(fontColor) {
    if(this.props.showEditStore) {
      return (
        <View style={{ flex: 1, alignItems: 'flex-end' }}>
          <TouchableOpacity onPress={() => { this.openEditStore(this.props.store._id) }}>
            <Icon name='store' size={25} color={fontColor} />
          </TouchableOpacity>
        </View>
      )
    }
    else
      return null
  }
  render() {
    console.log(this.props.store.imageUrl);
    var fontColor = this.props.fontColor ? this.props.fontColor : colors.black
    return (
      <View style={{ backgroundColor: this.props.backgroundColor, flexDirection: 'row', flex: 1, padding: 12 }}>
        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.openStoreProfile(this.props.store._id)}>
          <ProfileImage dimension={55} uri={this.props.store.imageUrl} />
        </TouchableOpacity>
        <View style={{ flex: 3 }}> 
          <Text style={{ color: fontColor, fontSize: 18, fontWeight: 'bold' }}>{ this.props.store.name }</Text>
          <IconAwesome style={{ paddingTop: 5 }} name='map-marker' size={13} color={colors.accent2}>  <Text style={{color: fontColor}}>{ this.props.store.address.city ? this.props.store.address.city : 'On earth' }</Text></IconAwesome>
          <IconAwesome style={{ paddingTop: 8 }} name='th-list' size={13} color={colors.accent2}>  <Text style={{color: fontColor}}>{ this.props.store.menus.length } Listings</Text></IconAwesome>
        </View>
        { this.renderEditStoreButton(fontColor) }
      </View>
    )
  }
}