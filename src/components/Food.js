import React from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  ActivityIndicator
} from 'react-native'
import Image from 'react-native-image-progress'
import IconAwesome from 'react-native-vector-icons/FontAwesome'
import IconBadge from 'react-native-icon-badge'
import { Actions } from 'react-native-router-flux'

import profileStyles from '../containers/Landing/styles/ProfileStyle'
import categoryStyles from './styles/CategoryStyle'
import styles from './styles/FoodStyle'
import { colors } from '../themes'

import utilities from '../utilities/utilities' 

import IconButton from './Buttons/IconButton'
import ProfileImage from './ProfileImage'

export default class Food extends React.Component {
  renderShopLabel() {
    if (this.props.showShopLabel)
      return (
        <View style={styles.shopProfileContainer}>
          <View style={{ flex: 6 }}>
            <View style={styles.shopProfile}>
              <ProfileImage dimension={45} uri={this.props.food.shop.profileImage} />
              <Text style={[profileStyles.profileName, styles.shopName]}>{ this.props.food.shop.name }</Text>
            </View>
          </View>
          <View style={styles.favoriteContainer}>
            { this.props.food.food.isFavorited ? <IconButton iconName='favorite' iconSize={18} iconColor={colors.accent2} /> : <IconButton iconName='favorite-border' iconSize={18} iconColor={colors.accent2} /> }
          </View>
        </View>
      )
  }
  render() {
    if(!this.props.food)
      return <ActivityIndicator style={styles.spinnerContainer} />

    if(this.props.food != 'empty'){
      return (
        <TouchableOpacity style={styles.container} onPress={() => Actions.foodDetails({foodId: this.props.food.food.id})}>
          <Image source={{uri: this.props.food.food.image}} resizeMode='cover' style={styles.foodImage} />
          <View style={categoryStyles.overlay}>
            <View style={styles.shopProfileContainer}>
              { this.renderShopLabel() }
            </View>
            <View style={styles.foodDetailsContainer}>
              <Text style={styles.foodName}>{ this.props.food.food.name }</Text>
              <View style={styles.foodPriceContainer}>
                <Text style={styles.foodPrice}>RM { this.props.food.food.currentPrice.toFixed(2) }</Text>
                { this.props.food.food.originalPrice ? <Text style={styles.foodOriginalPrice}>RM { this.props.food.food.originalPrice.toFixed(2) }</Text> : null }
                { this.props.food.food.originalPrice ? <Text style={[styles.foodPrice, styles.foodDiscount]}>-{utilities.calculateDiscount(this.props.food.food.currentPrice, this.props.food.food.originalPrice)}%</Text> : null }
              </View>
              <View style={styles.foodDetailsFooterContainer}>
                <View style={styles.foodRatingContainer}>
                  { utilities.renderRatingStar(this.props.food.food.rating, styles.foodRatingStar) }
                  <Text style={styles.foodRatingText}>{ this.props.food.food.ratingCount }</Text>
                </View>
                <IconAwesome style={styles.foodTime} name='clock-o' color={colors.white}> { this.props.food.food.time.toUpperCase() }</IconAwesome>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )
    }
    else
      return (
        <View style={styles.emptyContainer}>
        </View>
      )
  }
}