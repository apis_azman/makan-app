import React, { Component } from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal'
import ModalContent from '../../containers/ModalContent'

export default class ModalBox extends React.Component {
  render() {
    return (
      <View>
        <Modal isVisible={this.props.isModalVisible === 1}>
          <ModalContent 
            content={1}
            onClick={this.props.onPressNext}
            onClose={this.props.onPressClose}
          />
        </Modal>
        <Modal isVisible={this.props.isModalVisible === 2}>
          <ModalContent 
            content={2}
            onClick={this.props.onPressClose}
            onClose={this.props.onPressClose}
          />
        </Modal>
    </View>
    )
  }
}