import React from 'react'
import { View, Text, Button } from 'react-native'
import Modal from  'react-native-modal'

import TextButton from '../Buttons/TextButton'

import styles from './styles/AlertModalStyle'

export default class NoInternetConnectionModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = { showModal: this.props.visible }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ showModal: nextProps.visible })
  }
  hideModal = () => {
    this.setState({ showModal: false })
  }
  render() {
    return (
      <Modal isVisible={this.state.showModal}>
        <View style={styles.modalContainer}>
          <Text style={styles.message}>{ this.props.message }</Text>
          <TextButton label="Okay" action={this.hideModal} />
        </View>
      </Modal>
    )
  }
}
