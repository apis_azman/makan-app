import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  modalContainer: { 
    height: 130, // check again if somehow modal button tekeluar on other screen device then cari workaround lain
    borderRadius: 4, 
    padding: 20, 
    paddingBottom: 0, 
    backgroundColor: 'white' 
  },
  message: { 
    textAlign: 'center', 
    marginBottom: 20 
  }
})