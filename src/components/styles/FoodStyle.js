import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../themes/'

export default StyleSheet.create({
  spinnerContainer: {
    flex: 1, 
    margin: 2, 
    height: 250, 
    backgroundColor: colors.lightGrey
  },
  container: { 
    flex: 1, 
    margin: 2, 
    marginTop: 4, 
    height: 180, 
    backgroundColor: '#ddd' 
  },
  foodImage: { 
    height: 180, 
    flexDirection: 'column' 
  },
  shopProfileContainer: { 
    flex: 2,
    flexDirection: 'row', 
    padding: 5 
  },
  shopProfile: { 
    flexDirection: 'row',
    justifyContent: 'flex-start', 
    alignItems: 'center'
  },
  shopName: { 
    marginTop: 0,
    marginLeft: 5,
    fontSize: fonts.size.regular
  },
  favoriteContainer: { 
    flex: 1, 
    alignItems: 'flex-end', 
    height: 30 
  },
  foodDetailsContainer: { 
    backgroundColor: colors.semiTransparentDark, 
    flex: 1, 
    paddingRight: 5, 
    paddingLeft: 5, 
    paddingTop: 2, 
    paddingBottom: 2
  },
  foodName: { 
    fontWeight: 'bold', 
    color: colors.white, 
    flex: 1
  },
  foodPriceContainer: { 
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center' 
  },
  foodPrice: { 
    color: colors.accent2,
    fontSize: fonts.size.small
  },
  foodOriginalPrice: { 
    color: colors.white,
    fontSize: fonts.size.small,
    marginLeft: 10,
    textDecorationLine: 'line-through', 
    textDecorationStyle: 'solid'
  },
  foodDiscount: {
    marginLeft: 10
  },
  foodDetailsFooterContainer: { 
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  foodRatingContainer: {
    alignItems: 'center',
    flexDirection: 'row'
  },
  foodRatingStar: {
    fontSize: fonts.size.tiny,
    color: 'yellow'
  },
  foodRatingText: {
    marginLeft: 2,
    fontSize: fonts.size.tiny,
    color: colors.white
  },
  foodTime: {
    fontSize: fonts.size.tiny,
    color: colors.white
  },
  emptyContainer: {
    flex: 1
  }
})