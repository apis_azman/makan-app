import { StyleSheet } from 'react-native'

import { colors } from '../../themes'

export default StyleSheet.create({
  profileImage: {
    borderRadius: 50, 
    borderWidth: 2, 
    borderColor: colors.white
  },
  badge: {
    backgroundColor: colors.lightGreen
  }
})