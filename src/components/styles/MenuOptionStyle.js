import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../themes/'

export default StyleSheet.create({
  container: { 
    padding: 10 
  },
  optionButton: { 
    marginBottom: 2, 
    backgroundColor: 'white', 
    justifyContent: 'center', 
    height: 50, 
    borderWidth: 1, 
    borderColor: 'grey' 
  },
  optionLabel: { 
    textAlign: 'center', 
    fontWeight: '500' 
  }
})