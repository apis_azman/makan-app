import { StyleSheet } from 'react-native'

import { colors } from '../../themes/'

export default StyleSheet.create({
  container: { 
    paddingRight: 10, 
    paddingLeft: 10, 
    paddingTop: 20, 
    paddingBottom: 20,
    // commented to remove the double line effect
    // marginTop: 20,
    // borderBottomWidth: 1, 
    borderTopWidth: 1,
    borderColor: colors.lightGrey 
  }
})