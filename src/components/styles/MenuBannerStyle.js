import { StyleSheet } from 'react-native'

import { colors, fonts } from '../../themes'

export default StyleSheet.create({
  menuDetailsContainer: {
    flexDirection: 'row',
    height: 100,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGrey
  },
  menuImage: {
    flex: 2
  },
  menuDetails: {
    flex: 4,
    marginLeft: 10
  },
  menuTitleContainer: {
    justifyContent: 'center',
    flex: 1
  },
  menuTitle: {
    fontSize: fonts.size.h6,
    fontWeight: '500',
    color: colors.black
  },
  menuActions: {
    flex: 2,
    marginLeft: 10,
    flexDirection: 'row', 
    justifyContent: 'flex-end',
    alignItems: 'flex-start' 
  },
  menuPriceContainer: {
    flex: 2,
    justifyContent: 'flex-start'
  },
  menuPrice: {
    fontSize: 23,
    color: colors.accent2
  }
})