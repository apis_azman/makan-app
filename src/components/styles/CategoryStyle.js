import { StyleSheet } from 'react-native'

import { fonts, colors } from '../../themes/'

export default StyleSheet.create({
  container: { 
    flex: 1,
    margin: 2,
    height: 150,
    justifyContent: 'center', 
    alignItems: 'flex-start',
    backgroundColor: colors.lightGrey
  },
  emptyContainer: {
    flex: 1
  },
  labelContainer: { 
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 50
  },
  backgroundImg: {
    height: 150,
    flexDirection: 'row',
    alignItems: 'center'
  },
  label: { 
    fontFamily: fonts.type.bold,
    fontSize: fonts.size.h6, 
    color: colors.white,
    textShadowColor: colors.lightDark,
    textShadowOffset: { width: 1, height: 1 }
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.semiTransparentDark
  }
})