import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: { 
    marginTop: 10, 
    marginBottom: 10 
  },
  title: { 
    fontWeight: '500', 
    fontSize: 16 
  },
  rating: { 
    flexDirection: 'row', 
    marginTop: 5 
  },
  byUser: { 
    marginTop: 5, 
    fontSize: 13 
  },
  message: { 
    marginTop: 5, 
    fontWeight: '500' 
  },
  ratingStyle: {
    fontSize: 20,
    color: 'yellow'
  }
})