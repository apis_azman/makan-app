/*
  props
  dimension - number
  uri - string
  checked - bool
*/

import React from 'react'
import { Image, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconBadge from 'react-native-icon-badge'

import styles from './styles/ProfileImageStyle'
import { colors } from '../themes'

export default class ProfileImage extends React.Component {
  renderImage() {
    if(this.props.uri != undefined && this.props.uri != null && this.props.uri.length > 0) {
      return (
        <Image style={[styles.profileImage, { height: this.props.dimension, width: this.props.dimension }]} source={{uri: this.props.uri}} resizeMode='cover' />
      )
    } else {
      iconSize = this.props.dimension * 0.6
      if(this.props.type == 'user')
        icon = 'account'
      else
        icon = 'store'
      return (
        <View style={[styles.profileImage, { height: this.props.dimension, width: this.props.dimension, justifyContent: 'center', alignItems: 'center' }]}>
          <Icon name={icon} size={iconSize} color={colors.white} />
        </View>
      )
    }
  }
  render() {
    var badgeDimension = this.props.dimension/5
    if(this.props.checked)
      return (
        <IconBadge 
          MainElement={this.renderImage()}
          BadgeElement={<Icon name='check' size={15} color={colors.white} />}
          IconBadgeStyle={[styles.badge, { height: badgeDimension, width: badgeDimension }]}
        />
      )
    else
      return this.renderImage()
  }
}