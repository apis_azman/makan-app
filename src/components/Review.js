import React from 'react'
import { View, Text } from 'react-native'

import styles from './styles/ReviewStyle'

import utilities from '../utilities/utilities'

export default class Review extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{ this.props.title }</Text>
        <View style={styles.rating}>
          { utilities.renderRatingStar(this.props.rating, styles.ratingStyle) }
        </View>
        <Text style={styles.byUser}>by { this.props.user } on { this.props.date }</Text>
        <Text style={styles.message}>"{ this.props.message }"</Text>
      </View>
    )
  }
}