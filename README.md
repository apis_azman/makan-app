# :fries::hamburger::poultry_leg: Makan #

*Bila lapaq buat pa? Makan*

***

Powered by [React Native](https://facebook.github.io/react-native/)

### How to Setup ###

**1)** Setup [React Native](https://facebook.github.io/react-native/docs/getting-started.html) first if you haven't

**2)** git clone/download zip this project repo

**3)** cd into the cloned/extracted project directory

**4)** Run `npm install`

### How to Run ###

**1)** For android, setup AVD or use real device and connect through ADB. For Mac owners/ios, just use xCode

**2)** cd into the project directory

**3)** For android, run `react-native run-android`. For ios, run `react-native run-ios`

### Moar How tos ###

Check :point_right: [here](https://bitbucket.org/asticus/makan-app/src/b730763a224688af646da4ff22e087dbf0dca3df/docs/howtos/index.md?at=master&fileviewer=file-view-default) :point_left:

### Errors with Workarounds ###

Check :point_right: [here](https://bitbucket.org/apis_azman/makan-app/src/d197bf9a50b3faac4081986037b8cd245a8a7e4c/docs/issue-workarounds/index.md?at=master&fileviewer=file-view-default) :point_left:

### Project Structor ###

React-Native related dev works should only be done in `/src/`

`/src/` structure:
```
src
├── assets - external assets
│   └── fonts - external font types
|   └── images - external pictures, custom icons pn boleh masuk
├── components - 'dumb' components
│   └── styles - 'dumb' components style
├── configs - configurations
├── containers - 'smart' components
|   └── styles - 'smart' components style
├── data - dummy data utk testing
├── navigation - all of the stuffs related to navigation, including navigation bar components
|   └── styles - navigation related components style
├── redux - reduk
│   └── actions - aksi2 redux
│   └── reducers - redux reducers
├── sagas - reduk side efek
├── services - api call, read write etc.
├── themes - tema ex. colors, fonts
└── transforms - api -> transforms -> view
```